﻿$SolutionFolder = (Resolve-Path(Join-Path (Split-Path $script:MyInvocation.MyCommand.Path) "..\..")).Path

Import-Module ".\helpers.psm1" -DisableNameChecking
Import-Module ".\teamcity.psm1" -DisableNameChecking
Import-Module ".\versionbuilder.psm1" -DisableNameChecking

$BuildParameters = @{
    # Конфигурация сборки
    Configuration = "Debug"

    # Название файла солюшна
    Solution = "rb-analytics.sln"  
}

  # Конфигурация сборки
    $Configuration = $BuildParameters.Configuration
    
    $Artifacts = $SolutionFolder + "\output\$Configuration\artifacts"

    if((Test-Path $Artifacts) -eq $true)
    {
        Remove-Item $Artifacts -Recurse
    }

    New-Item $Artifacts -ItemType directory

# Загружаем скрипт с тасками на построение
. .\build.ps1