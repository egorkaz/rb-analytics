﻿
Framework 4.5.1

FormatTaskName (("-"*25) + "[{0}]" + ("-"*25))

task default -depends  BuildSolution

Task BuildSolution -Depends Build, Zip

Task Build -Depends Clean, PatchAssemblyInfo {   	 #    Write-Host Building RR.Gateway ($Configuration) -ForegroundColor Green
    Exec { msbuild $SolutionFolder\rb-analytics.sln /t:Build /p:Configuration=$Configuration /v:quiet }    
}

Task Clean {
  
    Write-Host Cleaning $SolutionFolder\RR.Gateway.sln -ForegroundColor Green
    Exec { msbuild $SolutionFolder\rb-analytics.sln /t:Clean /p:Configuration=$Configuration /v:quiet } 
}

Task Zip {
    Write-Host Ziping of RB Stats Managing Tools ($Configuration) -ForegroundColor Green
    Zip-Files $SolutionFolder\src\RB.Stats\RB.Stats.Db.Manage\bin\$Configuration $Artifacts"\RB.Stats.Db.Manage-$script:SemanticVersion.zip"
}


<# Загрузка номера версии сборки #>
Task FetchVersion {
    # Собираем номер версии
    $versionInfo = Get-VersionInfo
    $script:Version = $versionInfo.Version
    $script:SemanticVersion = $versionInfo.SemanticVersion

   Write-Host $script:Version
}

<# Изменение номера версии во всех AssemblyInfo.cs #>
task PatchAssemblyInfo -depends FetchVersion {
    if(-not (TeamCity-IsActive))
    {
        Write-Host "Will not patch AssemblyInfos because build is not running under TeamCity "
    }
    else
    {
        Write-Host "Patching AssemblyInfos with v$script:Version (v$script:SemanticVersion), '$Configuration'"
        Get-ChildItemToDepth $SolutionFolder\src -Filter "AssemblyInfo.cs" | 
            ForEach-Object { Patch-AssemblyInfo $_.FullName $script:Version $script:SemanticVersion $Configuration; }
    }
}




