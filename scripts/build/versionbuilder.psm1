﻿#region public members

<#
    .SINOPSIS
    Изменить номера версии в AssemblyInfo.cs
#>
function Patch-AssemblyInfo($assemblyInfo, $version, $semanticVersion, $configuration) {
    Write-Host "`t$assemblyInfo"

    $lines = (Get-Content $assemblyInfo).Split(@("`n"))

    $lines = PatchAssemblyAttribute $lines "System.Reflection.AssemblyVersionAttribute" "`"$version`""
    $lines = PatchAssemblyAttribute $lines "System.Reflection.AssemblyFileVersionAttribute" "`"$version`""
    $lines = PatchAssemblyAttribute $lines "System.Reflection.AssemblyInformationalVersionAttribute" "`"$semanticVersion`""
    $lines = PatchAssemblyAttribute $lines "System.Reflection.AssemblyCopyrightAttribute" "`"Copyright © NPRTS $([datetime]::Today.Year)`""
    $lines = PatchAssemblyAttribute $lines "System.Reflection.AssemblyCompanyAttribute" "`"NPRTS`""
    $lines = PatchAssemblyAttribute $lines "System.Reflection.AssemblyTrademarkAttribute" "`"NPRTS`""
    $lines = PatchAssemblyAttribute $lines "System.Reflection.AssemblyProductAttribute" "`"RTS Repository`""
    $lines = PatchAssemblyAttribute $lines "System.Reflection.AssemblyConfigurationAttribute" "`"$configuration`""

    $output = [string]::Join("`n", $lines)

    Set-Content -Path $assemblyInfo -Value $output
}

<#
    .SINOPSIS
        Получить информацию о версии проекта из git
#>
function Get-VersionInfo()
{
    Write-Host "Fetching build properties"

    $build  = GetBuildNumber
    $rev    = ""#Get-Revision
	$tag    = ""#GetLastTagName
    $branch = ""#Get-Branch -Normalize $true
    
    $major = 0
    $minor = 0
    
    $major = 0
    $minor = 0

    if(-not [string]::IsNullOrWhitespace($tag))
    {
        $regex = New-Object System.Text.RegularExpressions.Regex("^(|v|V)(?<major>[0-9]+)\.(?<minor>[0-9]+)(|\.[0-9]*)$")
        $match = $regex.Match($tag)
        if($match.Success)
        {
            $major = [int] $match.Groups["major"].Value
            $minor = [int] $match.Groups["minor"].Value
        }
    }

    
    Write-Host "Version info"
    Write-Host "============"
    #Write-Host "Build number       = $build"
    #Write-Host "Revision hash      = $rev"
    #Write-Host "Branch name (safe) = $branch"
    #Write-Host "Tag name           = $tag"
    #Write-Host "Major version      = $major"
    #Write-Host "Minor version      = $minor"
    
    $ver    = $build.Split('-')[0]
    #$ver    = "$major.$minor.$build"
    #$semver = GetSemanticVersion $major $minor $build $branch $rev
    
    #Write-Host "Version            = $ver"
    #Write-Host "Semantic version   = $semver"
    
    #return @{ Version = $ver; SemanticVersion = $semver }
    return @{ Version = $ver; SemanticVersion = $build }
}

<#
    .SINOPSIS
        Восстановить отсутствующий AssemblyInfo.cs
#>
function Restore-AssemblyInfo($projectDir)
{
    while($projectDir.EndsWith("`""))
    {
        $projectDir = $projectDir.Substring(0, $projectDir.Length - 1)
    }

    $path = Join-Path $projectDir "AssemblyInfo.cs"
    if((Test-Path $path -PathType Leaf))
    {
        Write-Host "File $path already exists"
        return
    }

    $path = Join-Path $projectDir "Properties/AssemblyInfo.cs"
    if((Test-Path $path -PathType Leaf))
    {
        Write-Host "File $path already exists"
        return
    }

    $directory = Join-Path $projectDir "Properties"
    if(-not (Test-Path $directory -PathType Container))
    {
        MkDir $directory | Out-Null
    }

    $path = Join-Path $directory "AssemblyInfo.cs"

    $assemblyInfo = @(
        "// ****************************************************************************",
        "// Этот файл был сгенерирован автоматически и его не нужно изменять руками",
        "// ****************************************************************************",
        "",
        "[assembly: System.Reflection.AssemblyCompany(`"`")]",
        "[assembly: System.Reflection.AssemblyCopyright(`"`")]",
        "[assembly: System.Reflection.AssemblyTrademark(`"`")]",
        "[assembly: System.Reflection.AssemblyVersion(`"0.0.0`")]",
        "[assembly: System.Reflection.AssemblyFileVersion(`"0.0.0`")]",
        "[assembly: System.Reflection.AssemblyInformationalVersion(`"0.0.0`")]")

    Set-Content $path $assemblyInfo

    Write-Host "Created default file $path"
}

<#
    .SINOPSIS
    Изменить номер версии в nuspec-файле
#>
function Patch-NuSpec($nuspecPath, $semanticVersion)
{
    [xml] $nuspec = Get-Content $nuspecPath
    $nuspec.package.metadata.version = $semanticVersion
    $nuspec.Save($nuspecPath)
}

<#
    .SINOPSIS
        Получить хеш ревизии
#>
function Get-Revision
{
    $rev = (& git rev-parse --short HEAD | Out-String).Trim()
    return $rev
}

<#
    .SINOPSIS
        Получить название ветки
#>
function Get-Branch($Normalize = $false)
{
    $branchName = (& git rev-parse --symbolic-full-name HEAD | Out-String).Trim()
    $branchName = $branchName.Replace("refs/heads/","")

    if($Normalize -eq $true)
    {
        $branchParts = [System.Text.RegularExpressions.Regex]::Split($branchName, "[^a-zA-Z0-9]")
        $branchName = ""
        foreach($part in $branchParts)
        {
            if($part.Length -gt 0)
            {
                $c = $part[0]
                if($branchName.Length -gt 0)
                {
                    $c = [char]::ToUpper($part[0])
                }
                else
                {
                    $c = [char]::ToLower($part[0])
                }

                $branchName += $c
                if($part.Length -gt 1)
                {
                    $branchName += $part.Substring(1).ToLower()
                }
            }
        }
    }

    return $branchName
}

<#
    .SINOPSIS
        Получить последний git тег
#>
function GetLastTagName
{
    $tags = (& git tag | Out-String).Split(@("`n"))
    for($i = $tags.Length - 1; $i -ge 0; $i--)
    {
        if(-not [string]::IsNullOrEmpty($tags[$i]))
        {
            return $tags[$i].Trim()
        }
    }

    return ""
}

<#
    .SINOPSIS
        Получить URL репозитория
#>
function Get-OriginUrl
{
    return (& git config --get remote.origin.url | Out-String).Trim()
}

Export-ModuleMember -Function Get-VersionInfo, Patch-AssemblyInfo, Restore-AssemblyInfo, Patch-NuSpec, Get-Revision, Get-Branch, Get-LastTag, Get-OriginUrl

#endregion

#region private members

function GetBuildNumber
{
    $buildNumber = [environment]::GetEnvironmentVariable("BUILD_NUMBER")
    if([string]::IsNullOrWhitespace($buildNumber))
    {
        return 0
    }
    
    return $buildNumber
}

function GetSemanticVersion($major, $minor, $build, $branch, $revision)
{
    $hasBranch   = [string]::IsNullOrWhitespace($branch)
    $hasRevision = [string]::IsNullOrWhitespace($revision)
    
    $semver = "$major.$minor.$build"
        
    if(-not [string]::IsNullOrWhitespace($branch))
    {
        $semver += "-$branch"
    }

    if(-not [string]::IsNullOrWhitespace($revision))
    {
        $semver += "-$revision"
    }

    return $semver
}

function PatchAssemblyAttribute([array] $lines, [string] $attributeTypeName, [string] $attributeContent)
{
    $patched = $false

    $possibleAttributeNames = @();
    $attributeName = $attributeTypeName.Substring($attributeTypeName.LastIndexOf(".") + 1)

    $possibleAttributeNames += $attributeTypeName
    $possibleAttributeNames += $attributeName

    if($attributeTypeName.EndsWith("Attribute"))
    {
        $possibleAttributeNames += $attributeTypeName.Substring(0, $attributeTypeName.Length - "Attribute".Length)
    }

    if($attributeName.EndsWith("Attribute"))
    {
        $possibleAttributeNames += $attributeName.Substring(0, $attributeName.Length - "Attribute".Length)
    }

    $resultingLine = "[assembly: $($attributeTypeName)($attributeContent)]"
            
    for($i = 0; $i -lt $lines.Length; $i++)
    {
        $line = $lines[$i]
        $matches = $false
        
        foreach($possibleAttributeName in $possibleAttributeNames)
        {
            $regex = "\[assembly\:\s*$possibleAttributeName\s*\(.*\)\s*\].*"
            if([System.Text.RegularExpressions.Regex]::IsMatch($line, $regex))
            {
                $matches = $true
                break
            }
        }
        
        if($matches)
        {
            Write-Host "`t`tPatched $attributeTypeName"
            $lines[$i] = $resultingLine
            return $lines
        }
    }
    Write-Host "`t`tCreated $attributeTypeName"
    $lines += $resultingLine
    return $lines
}

#endregion