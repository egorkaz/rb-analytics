﻿function Zip-Files($sourcedir, $zipfilename)
{
    if((Test-Path $zipfilename) -eq $true)
    {
        Remove-Item $zipfilename
    }

    Add-Type -Assembly System.IO.Compression.FileSystem
    $compressionLevel = [System.IO.Compression.CompressionLevel]::Optimal
    [System.IO.Compression.ZipFile]::CreateFromDirectory($sourcedir, $zipfilename, $compressionLevel, $false)

    Write-Host "Files and folders from '$sourcedir' have been packaged into '$zipfilename'"
}

function Get-ChildItemToDepth {
  Param(
    [String]$Path = $PWD,
    [String]$Filter = "*",
    [Byte]$ToDepth = 5,
    [Byte]$CurrentDepth = 0
  )
  $CurrentDepth++
  Get-ChildItem $Path | %{
    $_ | ?{ $_.Name -Like $Filter }

    If ($_.PsIsContainer) {
      If ($CurrentDepth -le $ToDepth) {

        # Callback to this function
        Get-ChildItemToDepth -Path $_.FullName -Filter $Filter -ToDepth $ToDepth -CurrentDepth $CurrentDepth

      } Else {

        Write-Debug $("Skipping GCI for Folder: $($_.FullName) " + `
          "(Why: Current depth $CurrentDepth vs limit depth $ToDepth)")

      }
    }
  }
}

Export-ModuleMember -Function Zip-Files
Export-ModuleMember -Function Get-ChildItemToDepth