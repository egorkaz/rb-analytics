﻿using System;
using FluentMigrator.Runner.Announcers;

namespace RB.Stats.Db
{
    internal sealed class NLogAnnouncer : Announcer
    {
        public override void Write(string message, bool escaped)
        {
            Database.Log.Info(message);
        }

        public override void Error(Exception exception)
        {
            Database.Log.Error(exception);
        }

        public override void Error(string message)
        {
            Database.Log.Error(message);
        }
    }
}
