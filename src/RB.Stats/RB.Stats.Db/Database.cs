﻿using System;
using System.Collections.Generic;

using System.Data;
using LinqToDB;
using LinqToDB.Data;
using LinqToDB.DataProvider;
using NLog;
using RB.Stats.Db.Objects;

namespace RB.Stats.Db
{
    /// <summary>
    /// Контекст БД
    /// </summary>
    public sealed class Database : DataConnection, IDatabase
    {
        internal static readonly ILogger Log = LogManager.GetLogger(typeof(Database).FullName);
        private const IsolationLevel DefaultIsolationLevel = IsolationLevel.Snapshot;

        internal Database(IDataProvider provider, IDbConnection connection)
            : base(provider, connection)
        { }

       
        public ITable<Party> Parties
        {
            get { return GetTable<Party>(); }
        }
        
        public ITable<MarketSector> MarketSectors
        {
            get { return GetTable<MarketSector>(); }
        }
        
        public ITable<InstrumentClass> InstrumentClasses
        {
            get { return GetTable<InstrumentClass>(); }
        }

        public ITable<Coin> Coins
        {
            get { return GetTable<Coin>(); }
        }

        public ITable<Equity> Equities
        {
            get { return GetTable<Equity>(); }
        }

        public ITable<UnsecuredLoan> UnsecuredLoans
        {
            get { return GetTable<UnsecuredLoan>(); }
        }

        /// <summary>
        ///     Начать транзакцию
        /// </summary>
        DataConnectionTransaction IDatabase.BeginTransaction()
        {
            return BeginTransaction(DefaultIsolationLevel);
        }

        /// <summary>
        ///     Начать транзакцию с указанием поведения при блокировке транзакции для подключения.
        /// </summary>
        DataConnectionTransaction IDatabase.BeginTransaction(IsolationLevel isolationLevel)
        {
            return BeginTransaction(isolationLevel);
        }

        /// <summary>
        ///     Выполнить SQL запрос
        /// </summary>
        IEnumerable<T> IDatabase.Query<T>(string sql, params DataParameter[] parameters)
        {
            return this.Query<T>(sql, parameters);
        }
    }
}
