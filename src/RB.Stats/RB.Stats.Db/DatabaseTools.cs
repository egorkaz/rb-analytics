﻿using System;
using System.Data;
using System.Data.SqlClient;
using FluentMigrator.Runner;
using FluentMigrator.Runner.Initialization;
using FluentMigrator.Runner.Processors;
using FluentMigrator.Runner.Processors.SqlServer;
using LinqToDB.Common;
using LinqToDB.DataProvider;
using LinqToDB.DataProvider.SqlServer;


namespace RB.Stats.Db
{
    /// <summary>
    /// Управление БД
    /// </summary>
    public sealed class DatabaseTools : IDatabaseTools
    {
        private readonly string _connectionString;
        private readonly DatabaseProviderType _providerType;
        private readonly IDataProvider _dataProvider;

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="connectionString">
        /// Строка соединения с БД
        /// </param>
        /// <param name="providerType">
        /// Тип БД
        /// </param>
        /// <param name="updateToLatestMigration">
        /// Обновится до последней миграции
        /// </param>
        public DatabaseTools(string connectionString, DatabaseProviderType providerType, bool updateToLatestMigration = false)
        {
            _connectionString = connectionString;
            _providerType = providerType;

            _dataProvider = CreateDataProvider();

            if (updateToLatestMigration)
            {
                MigrateToLatest();
            }

            //Configuration.Linq.AllowMultipleQuery = true;
        }

        /// <summary>
        /// Создать контекст БД
        /// </summary>
        /// <returns>
        /// Контекст БД
        /// </returns>
        public IDatabase Create()
        {
            var dbConnection = CreateDbConnection();
            
            return new Database(_dataProvider, dbConnection);
        }

        private void MigrateToLatest()
        {
            var announcer = new NLogAnnouncer { ShowSql = true };
            var assembly = typeof(Database).Assembly;

            var migrationContext = new RunnerContext(announcer)
            {
                ApplicationContext = CreateApplicationContext(_connectionString, _providerType)
            };

            var options = new ProcessorOptions { PreviewOnly = false, Timeout = 60, };
            var factory = CreateMigrationProcessorFactory();
            using (var processor = factory.Create(_connectionString, announcer, options))
            {
                var runner = new MigrationRunner(assembly, migrationContext, processor);
                runner.MigrateUp();
            }
        }
        
        public static IMigrationProcessorFactory GetMigrationProcessorFactory(DatabaseProviderType providerType)
        {
            switch (providerType)
            {
                case DatabaseProviderType.SqlServer2008:
                    return new SqlServer2008ProcessorFactory();
                case DatabaseProviderType.SqlServer2012:
                    return new SqlServer2012ProcessorFactory();
                default:
                    throw new ArgumentOutOfRangeException($"Unknown provider type: '{providerType}'");
            }
        }

        public static ApplicationContext CreateApplicationContext(string connectionString, DatabaseProviderType providerType)
        {
            switch (providerType)
            {
                case DatabaseProviderType.SqlServer2008:
                case DatabaseProviderType.SqlServer2012:
                    return new ApplicationContext
                    {
                        DatabaseName = new SqlConnectionStringBuilder(connectionString).InitialCatalog
                    };
                default:
                    throw new ArgumentOutOfRangeException($"Unknown provider type: '{providerType}'");
            }
        }

        private IMigrationProcessorFactory CreateMigrationProcessorFactory()
        {
            return GetMigrationProcessorFactory(_providerType);
        }

        private IDataProvider CreateDataProvider()
        {
            switch (_providerType)
            {
                case DatabaseProviderType.SqlServer2008:
                    return SqlServerTools.GetDataProvider(SqlServerVersion.v2008);
                case DatabaseProviderType.SqlServer2012:
                    return SqlServerTools.GetDataProvider(SqlServerVersion.v2012);
                default:
                    throw new ArgumentOutOfRangeException($"Unknown provider type: '{_providerType}'");
            }
        }

        private IDbConnection CreateDbConnection()
        {
            switch (_providerType)
            {
                case DatabaseProviderType.SqlServer2008:
                case DatabaseProviderType.SqlServer2012:
                    return new SqlConnection(_connectionString);
                default:
                    throw new ArgumentOutOfRangeException($"Unknown provider type: '{_providerType}'");
            }
        }
    }
}
