﻿namespace RB.Stats.Db
{
    /// <summary>
    ///     Тип БД
    /// </summary>
    public enum DatabaseProviderType
    {
        /// <summary>
        ///     MS SQL Server 2008
        /// </summary>
        SqlServer2008,

        /// <summary>
        ///     MS SQL Server 2008
        /// </summary>
        SqlServer2012
    }
}
