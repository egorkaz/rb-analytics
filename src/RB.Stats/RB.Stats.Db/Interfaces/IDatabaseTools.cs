﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RB.Stats.Db
{
    public interface IDatabaseTools
    {
        /// <summary>
        ///     Создать контекст БД
        /// </summary>
        /// <returns>
        ///     Контекст БД
        /// </returns>
        IDatabase Create();
    }
}
