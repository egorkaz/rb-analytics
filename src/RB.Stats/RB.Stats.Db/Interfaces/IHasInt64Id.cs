﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RB.Stats.Db
{
    /// <summary>
    /// Интерфейс используется в объектах у которых Primary Key имеет 64 битное значение
    /// </summary>
    internal interface IObjectInt64Id
    {
        /// <summary>
        /// Primary Key
        /// </summary>
        long Id { get; }
    }
}
