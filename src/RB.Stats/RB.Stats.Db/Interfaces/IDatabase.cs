﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using RB.Stats.Db.Objects;
using System.Data;
using LinqToDB;
using LinqToDB.Data;

namespace RB.Stats.Db
{
    /// <summary>
    /// Контекст Базы данных
    /// </summary>
    public interface IDatabase : IDataContext
    {         
        ITable<Party> Parties { get; }
        
        ITable<MarketSector> MarketSectors { get; }
        
        ITable<InstrumentClass> InstrumentClasses { get; }

        ITable<Coin> Coins { get; }

        ITable<Equity> Equities { get; }

        ITable<UnsecuredLoan> UnsecuredLoans { get; }

        /// <summary>
        /// Соединение с БД
        /// </summary>        
        IDbConnection Connection { get; }

        /// <summary>
        /// Транзакция
        /// </summary>
        IDbTransaction Transaction { get; }

        /// <summary>
        /// Начать транзакцию
        /// </summary>
        DataConnectionTransaction BeginTransaction();

        /// <summary>
        /// Начать транзакцию
        /// </summary>
        DataConnectionTransaction BeginTransaction(IsolationLevel isolationLevel);

        /// <summary>
        /// Выполнить SQL запрос
        /// </summary>
        IEnumerable<T> Query<T>(string sql, params DataParameter[] parameters);
    }
}
