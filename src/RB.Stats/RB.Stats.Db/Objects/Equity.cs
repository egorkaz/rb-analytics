﻿using LinqToDB;
using LinqToDB.Mapping;
using System;

namespace RB.Stats.Db.Objects
{
    [Table("Equities")]
    public class Equity : IObjectInt32Id
    {
        public const string FK_Equity_To_InstrumentClass = "FK_Equity_To_InstrumentClass";

        /// <summary>
        /// PK
        /// </summary>
        [Column("Id", IsIdentity = true, IsPrimaryKey = true, CanBeNull = false)]
        public int Id { get; set; }

        /// <summary>
        /// EquityCode
        /// </summary>
        [Column("EquityCode", DataType = DataType.NVarChar, Length = 64, CanBeNull = false)]
        public string EquityCode { get; set; }

        /// <summary>
        /// NameRu
        /// </summary>
        [Column("NameRu", CanBeNull = false)]
        public string NameRu { get; set; }

        /// <summary>
        /// NameEn
        /// </summary>
        [Column("NameEn", CanBeNull = false)]
        public string NameEn { get; set; }

        /// <summary>
        /// Isin
        /// </summary>
        [Column("Isin", CanBeNull = true)]
        public string Isin { get; set; }

        /// <summary>
        /// Cfi
        /// </summary>
        [Column("Cfi", CanBeNull = true)]
        public string Cfi { get; set; }

        /// <summary>
        /// Cfi
        /// </summary>
        [Column("Regnum", CanBeNull = true)]
        public string Regnum { get; set; }

        /// <summary>
        /// UnitNominal
        /// </summary>
        [Column("UnitNominal", CanBeNull = false, DataType = DataType.Decimal, Length = 18, Precision = 5)]
        public decimal UnitNominal { get; set; }

        /// <summary>
        /// Currency
        /// </summary>
        [Column("Currency", CanBeNull = false, DataType = DataType.NVarChar, Length = 3)]
        public string Currency { get; set; }

        /// <summary>
        /// IssuingVolumes
        /// </summary>
        [Column("IssuingVolumes", CanBeNull = false)]
        public long IssuingVolumes { get; set; }

        /// <summary>
        /// FK, Идентификатор сектора рынка к которому относится класс
        /// </summary>
        [Column("InstrumentClassId", CanBeNull = false)]
        public int InstrumentClassId { get; set; }

        /// <summary>
        /// Ссылка на класс инструмента
        /// </summary>
        [Association(ThisKey = "InstrumentClassId", OtherKey = "Id", CanBeNull = false)]
        public InstrumentClass InstrumentClass { get; set; }

        /// <summary>
        /// IsActive
        /// </summary>
        [Column("IsActive", CanBeNull = false, DataType = DataType.Boolean)]
        public bool IsActive { get; set; }
    }
}
