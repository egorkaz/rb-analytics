﻿using LinqToDB;
using LinqToDB.Mapping;

namespace RB.Stats.Db.Objects
{
    [Table("InstrumentClasses")]
    public class InstrumentClass : IObjectInt32Id
    {
        public const string FK_InstrumentClass_To_MarketSector = "FK_InstrumentClass_To_MarketSector";

        /// <summary>
        /// PK
        /// </summary>
        [Column("Id", IsIdentity = true, IsPrimaryKey = true, CanBeNull = false)]
        public int Id { get; set; }

        /// <summary>
        /// ClassCode
        /// </summary>
        [Column("ClassCode", DataType = DataType.NVarChar, Length = 64, CanBeNull = false)]
        public string ClassCode { get; set; }

        /// <summary>
        /// NameRu
        /// </summary>
        [Column("NameRu", CanBeNull = false)]
        public string NameRu { get; set; }

        /// <summary>
        /// NameEn
        /// </summary>
        [Column("NameEn", CanBeNull = false)]
        public string NameEn { get; set; }


        /// <summary>
        /// FK, Идентификатор сектора рынка к которому относится класс
        /// </summary>
        [Column("MarketSectorId", CanBeNull = false)]
        public int MarketSectorId { get; set; }

        /// <summary>
        /// Ссылка на сектор рынка
        /// </summary>
        [Association(ThisKey = "MarketSectorId", OtherKey = "Id", CanBeNull = false)]
        public MarketSector MarketSector { get; set; }
        
        /// <summary>
        /// IsActive
        /// </summary>
        [Column("IsActive", CanBeNull = false, DataType = DataType.Boolean)]
        public bool IsActive { get; set; }
    }
}
