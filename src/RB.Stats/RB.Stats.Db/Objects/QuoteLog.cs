﻿using LinqToDB;
using LinqToDB.Mapping;
using System;

namespace RB.Stats.Db.Objects
{
    [Table("QuoteLogs")]
    public class QuoteLog : IObjectInt64Id
    {
        public const string FK_Quote_To_Instrument = "FK_Quote_To_Instrument";
        public const string FK_Quote_To_Party = "FK_Quote_To_Party";

        /// <summary>
        /// PK
        /// </summary>
        [Column("Id", IsIdentity = true, IsPrimaryKey = true, CanBeNull = false)]
        public long Id { get; set; }

        /// <summary>
        /// FK, Идентификатор инструмента
        /// </summary>
        [Column("InstrumnetId", CanBeNull = false)]
        public int InstrumnetId { get; set; }
        
        /// <summary>
        /// FK, Идентификатор владельца котировки
        /// </summary>
        [Column("PartyId", CanBeNull = false)]
        public int PartyId { get; set; }

        /// <summary>
        /// QuoteId
        /// </summary>
        [Column("QuoteId", DataType = DataType.NVarChar, Length = 42, CanBeNull = false)]
        public string QuoteId { get; set; }

        /// <summary>
        /// ActiveDateTime
        /// </summary>
        [Column("ActiveDateTime", CanBeNull = false)]
        public DateTime ActiveDateTime { get; set; }

        /// <summary>
        /// ActiveRevisionId
        /// </summary>
        [Column("ActiveRevisionId", CanBeNull = false)]
        public int ActiveRevisionId { get; set; }

        /// <summary>
        /// CancelledDateTime
        /// </summary>
        [Column("CancelledDateTime", CanBeNull = true)]
        public DateTime CancelledDateTime { get; set; }

        /// <summary>
        /// CancelledRevisionId
        /// </summary>
        [Column("CancelledRevisionId", CanBeNull = true)]
        public int CancelledRevisionId { get; set; }

        /// <summary>
        /// Direction
        /// </summary>
        [Column("Direction", DataType = DataType.NChar, Length = 1, CanBeNull = false)]
        public QuoteDirection Direction { get; set; }

        /// <summary>
        /// Price
        /// </summary>
        [Column("Price", DataType = DataType.Decimal, Length = 18, Precision = 5, CanBeNull = false)]
        public decimal Price { get; set; }

        /// <summary>
        /// PriceCurrency
        /// </summary>
        [Column("PriceCurrency", DataType = DataType.NVarChar, Length = 3, CanBeNull = false)]
        public string PriceCurrency { get; set; }

        /// <summary>
        /// Quantity
        /// </summary>
        [Column("Quantity", DataType = DataType.Decimal, Length = 18, Precision = 5, CanBeNull = false)]
        public decimal Quantity { get; set; }

    }
}
