﻿using LinqToDB;
using LinqToDB.Mapping;

namespace RB.Stats.Db.Objects
{
    [Table("Parties")]
    public class Party : IObjectInt32Id
    {
        /// <summary>
        /// PK
        /// </summary>
        [Column("Id", IsIdentity = true, IsPrimaryKey = true, CanBeNull = false)]
        public int Id { get; set; }

        /// <summary>
        /// PartyCode
        /// </summary>
        [Column("PartyCode", DataType = DataType.NVarChar, Length = 12, CanBeNull = false)]
        public string PartyCode { get; set; }

        /// <summary>
        /// ShortNameRu
        /// </summary>
        [Column("ShortNameRu", CanBeNull = false)]
        public string ShortNameRu { get; set; }

        /// <summary>
        /// ShortNameEn
        /// </summary>
        [Column("ShortNameEn", CanBeNull = false)]
        public string ShortNameEn { get; set; }

        /// <summary>
        /// FullNameRu
        /// </summary>
        [Column("FullNameRu", CanBeNull = false)]
        public string FullNameRu { get; set; }

        /// <summary>
        /// FullNameEn
        /// </summary>
        [Column("FullNameEn", CanBeNull = false)]
        public string FullNameEn { get; set; }

        /// <summary>
        /// PostalAddress
        /// </summary>
        [Column("PostalAddress", CanBeNull = true)]
        public string PostalAddress { get; set; }

        /// <summary>
        /// PostalAddress
        /// </summary>
        [Column("LegalAddress", CanBeNull = true)]
        public string LegalAddress { get; set; }

        /// <summary>
        /// Email
        /// </summary>
        [Column("Email", CanBeNull = true)]
        public string Email { get; set; }

        /// <summary>
        /// Phone
        /// </summary>
        [Column("Phone", CanBeNull = true)]
        public string Phone { get; set; }

        /// <summary>
        /// Country
        /// </summary>
        [Column("Country", CanBeNull = true)]
        public string Country { get; set; }

        /// <summary>
        /// Jurisdiction
        /// </summary>
        [Column("Jurisdiction", CanBeNull = true)]
        public string Jurisdiction { get; set; }

        /// <summary>
        /// OrganizationType
        /// </summary>
        [Column("OrganizationType", CanBeNull = true)]
        public string OrganizationType { get; set; }
    }
}
