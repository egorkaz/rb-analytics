﻿using LinqToDB.Mapping;

namespace RB.Stats.Db.Objects
{
    public enum QuoteDirection
    {
        [MapValue("B")]
        Buy,
        [MapValue("S")]
        Sell
    }
}
