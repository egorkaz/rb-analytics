﻿using LinqToDB;
using LinqToDB.Mapping;
using System;

namespace RB.Stats.Db.Objects
{
    [Table("MarketTrades")]
    public class MarketTrade : IObjectInt64Id
    {
        public const string FK_MarketTrade_To_Instrument = "FK_MarketTrade_To_Instrument";

        /// <summary>
        /// PK
        /// </summary>
        [Column("Id", IsIdentity = true, IsPrimaryKey = true, CanBeNull = false)]
        public long Id { get; set; }

        /// <summary>
        /// MarketTradeId от RTS Board
        /// </summary>
        [Column("MarketTradeId", CanBeNull = false)]
        public long MarketTradeId { get; set; }

        /// <summary>
        /// FK, Идентификатор инструмента
        /// </summary>
        [Column("InstrumnetId", CanBeNull = false)]
        public int InstrumnetId { get; set; }

        /// <summary>
        /// DateTime
        /// </summary>
        [Column("DateTime", CanBeNull = true)]
        public DateTime DateTime { get; set; }

        /// <summary>
        /// DescriptionRu
        /// </summary>
        [Column("DescriptionRu", CanBeNull = false)]
        public string DescriptionRu { get; set; }

        /// <summary>
        /// DescriptionEn
        /// </summary>
        [Column("DescriptionEn", CanBeNull = false)]
        public string DescriptionEn { get; set; }

        /// <summary>
        /// Price
        /// </summary>
        [Column("Price", DataType = DataType.Decimal, Length = 18, Precision = 5, CanBeNull = false)]
        public decimal Price { get; set; }

        /// <summary>
        /// PriceCurrency
        /// </summary>
        [Column("PriceCurrency", DataType = DataType.NVarChar, Length = 3, CanBeNull = false)]
        public string PriceCurrency { get; set; }

        /// <summary>
        /// Quantity
        /// </summary>
        [Column("Quantity", DataType = DataType.Decimal, Length = 18, Precision = 5, CanBeNull = false)]
        public decimal Quantity { get; set; }
    }
}
