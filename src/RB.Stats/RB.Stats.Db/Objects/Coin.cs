﻿using LinqToDB;
using LinqToDB.Mapping;
using System;

namespace RB.Stats.Db.Objects
{
    [Table("Coins")]
    public class Coin : IObjectInt32Id
    {
        public const string FK_Coin_To_InstrumentClass = "FK_Coin_To_InstrumentClass";

        /// <summary>
        /// PK
        /// </summary>
        [Column("Id", IsIdentity = true, IsPrimaryKey = true, CanBeNull = false)]
        public int Id { get; set; }

        /// <summary>
        /// CoinCode
        /// </summary>
        [Column("CoinCode", DataType = DataType.NVarChar, Length = 64, CanBeNull = false)]
        public string CoinCode { get; set; }

        /// <summary>
        /// NameRu
        /// </summary>
        [Column("NameRu", CanBeNull = false)]
        public string NameRu { get; set; }

        /// <summary>
        /// NameEn
        /// </summary>
        [Column("NameEn", CanBeNull = false)]
        public string NameEn { get; set; }

        /// <summary>
        /// CatalogueNumber
        /// </summary>
        [Column("CatalogueNumber", CanBeNull = false)]
        public string CatalogueNumber { get; set; }

        /// <summary>
        /// Series
        /// </summary>
        [Column("Series", CanBeNull = true)]
        public string Series { get; set; }
        
        /// <summary>
        /// NominalValue
        /// </summary>
        [Column("NominalValue", CanBeNull = false, DataType = DataType.Decimal, Length = 18, Precision = 5)]
        public decimal NominalValue { get; set; }

        /// <summary>
        /// NominalCurrency
        /// </summary>
        [Column("NominalCurrency", CanBeNull = false, DataType = DataType.NVarChar, Length = 3)]
        public string NominalCurrency { get; set; }

        /// <summary>
        /// Quality
        /// </summary>
        [Column("Quality", CanBeNull = false)]
        public string Quality { get; set; }

        /// <summary>
        /// MetalType
        /// </summary>
        [Column("MetalType", CanBeNull = false)]
        public string MetalType { get; set; }

        /// <summary>
        /// MetalAssay
        /// </summary>
        [Column("MetalAssay", CanBeNull = false)]
        public string MetalAssay { get; set; }

        /// <summary>
        /// ChemicallyPureMetalContent
        /// </summary>
        [Column("ChemicallyPureMetalContent", CanBeNull = true)]
        public string ChemicallyPureMetalContent { get; set; }

        /// <summary>
        /// Weight
        /// </summary>
        [Column("Weight", CanBeNull = false)]
        public string Weight { get; set; }

        /// <summary>
        /// Diameter
        /// </summary>
        [Column("Diameter", CanBeNull = true)]
        public string Diameter { get; set; }

        /// <summary>
        /// Thickness
        /// </summary>
        [Column("Thickness", CanBeNull = true)]
        public string Thickness { get; set; }

        /// <summary>
        /// IssuingVolumes
        /// </summary>
        [Column("IssuingVolumes", CanBeNull = true)]
        public string IssuingVolumes { get; set; }

        /// <summary>
        /// IssueDate
        /// </summary>
        [Column("IssueDate", CanBeNull = false, DataType = DataType.Date)]
        public DateTime IssueDate { get; set; }

        /// <summary>
        /// Mint
        /// </summary>
        [Column("Mint", CanBeNull = false)]
        public string Mint { get; set; }

        /// <summary>
        /// AdditionalInformation
        /// </summary>
        [Column("AdditionalInformation", CanBeNull = true)]
        public string AdditionalInformation { get; set; }

        /// <summary>
        /// CbrUrl
        /// </summary>
        [Column("CbrUrl", CanBeNull = false)]
        public string CbrUrl { get; set; }

        /// <summary>
        /// FK, Идентификатор сектора рынка к которому относится класс
        /// </summary>
        [Column("InstrumentClassId", CanBeNull = false)]
        public int InstrumentClassId { get; set; }

        /// <summary>
        /// Ссылка на класс инструмента
        /// </summary>
        [Association(ThisKey = "InstrumentClassId", OtherKey = "Id", CanBeNull = false)]
        public InstrumentClass InstrumentClass { get; set; }

        /// <summary>
        /// IsActive
        /// </summary>
        [Column("IsActive", CanBeNull = false, DataType = DataType.Boolean)]
        public bool IsActive { get; set; }
    }
}