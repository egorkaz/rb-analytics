﻿using LinqToDB;
using LinqToDB.Mapping;
using System;

namespace RB.Stats.Db.Objects
{
    [Table("UnsecuredLoans")]
    public class UnsecuredLoan : IObjectInt32Id
    {
        public const string FK_UnsecuredLoan_To_InstrumentClass = "FK_UnsecuredLoan_To_InstrumentClass";

        /// <summary>
        /// PK
        /// </summary>
        [Column("Id", IsIdentity = true, IsPrimaryKey = true, CanBeNull = false)]
        public int Id { get; set; }

        /// <summary>
        /// UnsecuredLoanCode
        /// </summary>
        [Column("UnsecuredLoanCode", DataType = DataType.NVarChar, Length = 64, CanBeNull = false)]
        public string UnsecuredLoanCode { get; set; }

        /// <summary>
        /// NameRu
        /// </summary>
        [Column("NameRu", CanBeNull = false)]
        public string NameRu { get; set; }

        /// <summary>
        /// NameEn
        /// </summary>
        [Column("NameEn", CanBeNull = false)]
        public string NameEn { get; set; }

        /// <summary>
        /// Currency
        /// </summary>
        [Column("Currency", CanBeNull = false, DataType = DataType.NVarChar, Length = 3)]
        public string Currency { get; set; }

        /// <summary>
        /// FK, Идентификатор сектора рынка к которому относится класс
        /// </summary>
        [Column("InstrumentClassId", CanBeNull = false)]
        public int InstrumentClassId { get; set; }

        /// <summary>
        /// Ссылка на класс инструмента
        /// </summary>
        [Association(ThisKey = "InstrumentClassId", OtherKey = "Id", CanBeNull = false)]
        public InstrumentClass InstrumentClass { get; set; }

        /// <summary>
        /// IsActive
        /// </summary>
        [Column("IsActive", CanBeNull = false, DataType = DataType.Boolean)]
        public bool IsActive { get; set; }
    }
        
}
