﻿using LinqToDB;
using LinqToDB.Mapping;

namespace RB.Stats.Db.Objects
{
    [Table("MarketSectors")]
    public class MarketSector : IObjectInt32Id
    {
        /// <summary>
        /// PK
        /// </summary>
        [Column("Id", IsIdentity = true, IsPrimaryKey = true, CanBeNull = false)]
        public int Id { get; set; }

        /// <summary>
        /// SectorCode
        /// </summary>
        [Column("SectorCode", DataType = DataType.NVarChar, Length = 64, CanBeNull = false)]
        public string SectorCode { get; set; }

        /// <summary>
        /// NameRu
        /// </summary>
        [Column("NameRu", CanBeNull = false)]
        public string NameRu { get; set; }

        /// <summary>
        /// NameEn
        /// </summary>
        [Column("NameEn", CanBeNull = false)]
        public string NameEn { get; set; }

        /// <summary>
        /// IsActive
        /// </summary>
        [Column("IsActive", CanBeNull = false, DataType = DataType.Boolean)]
        public bool IsActive { get; set; }
    }
}
