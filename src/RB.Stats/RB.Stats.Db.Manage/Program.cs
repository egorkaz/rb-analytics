﻿using System;
using System.Reflection;
using NLog;
using NLog.Config;
using NLog.Targets;
using PowerArgs;

namespace RB.Stats.Db.Manage
{
    class Program
    {
        private static readonly Version Version;

        static Program()
        {
            var fileVersionAttribute = typeof(Program).Assembly.GetCustomAttribute<AssemblyFileVersionAttribute>();
            if (fileVersionAttribute != null)
            {
                Version = Version.Parse(fileVersionAttribute.Version);
            }
            else
            {
                Version = new Version();
            }
        }

        static void Main(string[] args)
        {
            ConfigureLogger();

            ConsoleProvider.Current.Write("RB Stats Database tools ".ToConsoleString(ConsoleColor.Cyan));
            ConsoleProvider.Current.Write($"v{Version}".ToConsoleString());
            ConsoleProvider.Current.WriteLine();
            ConsoleProvider.Current.WriteLine();

            try
            {
                var action = Args.ParseAction<ProgramCommands>(args);
                if (action.HandledException != null)
                {
                    throw action.HandledException;
                }

                action.Invoke();

                ConsoleProvider.Current.WriteLine();
                return;
            }
            catch (ArgException e)
            {
                PrintErrorMessage(e);
                return;

            }
            catch (AggregateException e)
            {
                foreach (var ex in e.InnerExceptions)
                {
                    var argException = ex as ArgException;
                    if (argException != null)
                    {
                        PrintErrorMessage(argException);
                        continue;
                    }

                    ConsoleProvider.Current.WriteLine("Fatal error!".ToConsoleString(ConsoleColor.White, ConsoleColor.DarkRed));
                    ConsoleProvider.Current.WriteLine(ex.ToString().ToConsoleString(ConsoleColor.Red));
                }
                return;
            }
            catch (Exception e)
            {
                ConsoleProvider.Current.WriteLine("Fatal error!".ToConsoleString(ConsoleColor.White, ConsoleColor.DarkRed));
                ConsoleProvider.Current.WriteLine(e.ToString().ToConsoleString(ConsoleColor.Red));
                return;
            }
        }

        /// <summary>
        /// Конфигурим логгер
        /// </summary>
        private static void ConfigureLogger()
        {
            var config = new LoggingConfiguration();

            var target = new ColoredConsoleTarget
            {
                Layout = "${message}"
            };
            config.AddTarget(target.GetType().FullName, target);
            config.LoggingRules.Add(new LoggingRule("*", LogLevel.Trace, target));

            LogManager.Configuration = config;
            LogManager.ReconfigExistingLoggers();
        }

        private static void PrintErrorMessage(ArgException e)
        {
            ConsoleProvider.Current.WriteLine($"Invalid command line arguments! {e.Message}".ToConsoleString(ConsoleColor.Red));
            PrintUsage();
        }

        private static void PrintUsage()
        {
            ConsoleProvider.Current.Write("Type ");
            ConsoleProvider.Current.Write("rrsdb help".ToConsoleString(ConsoleColor.Cyan));
            ConsoleProvider.Current.WriteLine(" to get help on command line arguments.".ToConsoleString(ConsoleColor.Red));
        }
    }
}
