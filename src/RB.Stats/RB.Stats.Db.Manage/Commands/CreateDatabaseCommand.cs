﻿using System;
using System.Data.SqlClient;
using PowerArgs;

namespace RB.Stats.Db.Manage.Commands
{
    public sealed class CreateDatabaseCommand : CommandBase
    {
        public void Execute()
        {
            switch (ProviderType)
            {
                case DatabaseProviderType.SqlServer2008:
                case DatabaseProviderType.SqlServer2012:
                    {
                        var sb = new SqlConnectionStringBuilder(ConnectionString);
                        var dbName = sb.InitialCatalog;
                        sb.InitialCatalog = "master";
                        using (var connection = new SqlConnection(sb.ConnectionString))
                        {
                            connection.Open();

                            ConsoleProvider.Current.Write("Creating database ");
                            ConsoleProvider.Current.WriteLine(dbName.ToConsoleString(ConsoleColor.Cyan));
                            using (var command = connection.CreateCommand())
                            {
                                command.CommandText = $"CREATE DATABASE [{dbName}]";
                                command.ExecuteNonQuery();
                            }

                            ConsoleProvider.Current.WriteLine("Success".ToConsoleString(ConsoleColor.Green));                           
                        }
                    }
                    break;

                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}
