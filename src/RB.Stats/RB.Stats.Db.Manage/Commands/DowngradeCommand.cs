﻿using System;
using FluentMigrator.Runner;
using PowerArgs;

namespace RB.Stats.Db.Manage.Commands
{
    public sealed class DowngradeCommand : MigrationCommand
    {
        [ArgPosition(1)]
        [ArgShortcut("ver")]
        [ArgShortcut(ArgShortcutPolicy.ShortcutsOnly)]
        [ArgRequired]
        [ArgDescription("Target version")]
        public long Version { get; set; }

        protected override void ExecuteImplement(MigrationRunner runner)
        {
            ConsoleProvider.Current.Write("Reverting database to version ");
            ConsoleProvider.Current.Write(Version.ToString().ToConsoleString(ConsoleColor.Cyan));
            ConsoleProvider.Current.Write("\n");
            runner.MigrateDown(Version);

            ConsoleProvider.Current.Write("Success\n".ToConsoleString(ConsoleColor.Green));
        }
    }
}
