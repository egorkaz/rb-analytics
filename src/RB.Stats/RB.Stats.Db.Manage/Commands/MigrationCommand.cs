﻿using System;
using System.Collections.Generic;
using FluentMigrator.Runner;
using FluentMigrator.Runner.Announcers;
using FluentMigrator.Runner.Initialization;
using FluentMigrator.Runner.Processors;
using PowerArgs;

namespace RB.Stats.Db.Manage.Commands
{
    /// <summary>
    /// Определяем команду для миграции
    /// </summary>
    public abstract class MigrationCommand : CommandBase
    {
        [ArgShortcut("preview")]
        [ArgShortcut(ArgShortcutPolicy.ShortcutsOnly)]
        [ArgDescription("Preview database changes.")]
        [DefaultValue(false)]
        public bool PreviewOnly { get; set; }

        public void Execute()
        {
            var announcer = new ConsoleAnnouncer
            {
                ShowSql = true,
                ShowElapsedTime = true
            };
            var assembly = typeof(Migrations.EmptyMigration).Assembly;

            var migrationContext = new RunnerContext(announcer)
            {
                ApplicationContext = DatabaseTools.CreateApplicationContext(ConnectionString, ProviderType)
            };

            var options = new ProcessorOptions
            {
                PreviewOnly = PreviewOnly,
                Timeout = Timeout
            };

            var factory = DatabaseTools.GetMigrationProcessorFactory(ProviderType);
            using (var processor = factory.Create(ConnectionString, announcer, options))
            {
                var runner = new MigrationRunner(assembly, migrationContext, processor);
                ExecuteImplement(runner);
            }
        }


        protected abstract void ExecuteImplement(MigrationRunner runner);
    }
}
