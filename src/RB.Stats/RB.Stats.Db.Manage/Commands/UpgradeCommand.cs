﻿using System;
using FluentMigrator.Runner;
using PowerArgs;

namespace RB.Stats.Db.Manage.Commands
{
    public sealed class UpgradeCommand : MigrationCommand
    {
        [ArgPosition(1)]
        [ArgShortcut("ver")]
        [ArgShortcut(ArgShortcutPolicy.ShortcutsOnly)]
        [ArgDescription("Target version")]
        public long? Version { get; set; }

        protected override void ExecuteImplement(MigrationRunner runner)
        {
            if (Version != null)
            {
                ConsoleProvider.Current.Write("Updating database to version ");
                ConsoleProvider.Current.Write(Version.Value.ToString().ToConsoleString(ConsoleColor.Cyan));
                ConsoleProvider.Current.Write("\n");
                runner.MigrateUp(Version.Value);
            }
            else
            {
                ConsoleProvider.Current.Write("Updating database to");
                ConsoleProvider.Current.Write(" latest ".ToConsoleString(ConsoleColor.Cyan));
                ConsoleProvider.Current.Write("version\n");
                runner.MigrateUp();
            }

            ConsoleProvider.Current.Write("Success\n".ToConsoleString(ConsoleColor.Green));
        }
    }
}
