﻿using System;
using System.Data.SqlClient;
using PowerArgs;

namespace RB.Stats.Db.Manage.Commands
{
    public sealed class CreateUserCommand : CommandBase
    {
        [ArgShortcut("name")]
        [ArgShortcut(ArgShortcutPolicy.ShortcutsOnly)]
        [ArgPosition(1)]
        [ArgDefaultValue(@"NT AUTHORITY\SYSTEM")]
        [ArgDescription("Windows user name")]
        public string UserName { get; set; }

        public void Execute()
        {
            switch (ProviderType)
            {
                case DatabaseProviderType.SqlServer2008:
                case DatabaseProviderType.SqlServer2012:
                    {
                        var sb = new SqlConnectionStringBuilder(ConnectionString);
                        var dbName = sb.InitialCatalog;
                        using (var connection = new SqlConnection(ConnectionString))
                        {
                            connection.Open();

                            ConsoleProvider.Current.Write("Adding user ");
                            ConsoleProvider.Current.WriteLine(UserName.ToConsoleString(ConsoleColor.Cyan));
                            ConsoleProvider.Current.Write(" to database ");
                            ConsoleProvider.Current.WriteLine(dbName.ToConsoleString(ConsoleColor.Cyan));
                            using (var command = connection.CreateCommand())
                            {
                                command.CommandText = $"CREATE USER [{UserName}] FOR LOGIN  [{UserName}]";
                                command.ExecuteNonQuery();
                            }

                            foreach (var role in new[] { "db_datareader", "db_datawriter" })
                            {
                                ConsoleProvider.Current.Write("Adding role ");
                                ConsoleProvider.Current.WriteLine(role.ToConsoleString(ConsoleColor.Cyan));
                                ConsoleProvider.Current.Write(" to user ");
                                ConsoleProvider.Current.WriteLine(UserName.ToConsoleString(ConsoleColor.Cyan));
                                using (var command = connection.CreateCommand())
                                {
                                    command.CommandText = $"ALTER ROLE [{role}] ADD MEMBER [{UserName}]";
                                    command.ExecuteNonQuery();
                                }
                            }

                            ConsoleProvider.Current.WriteLine("Success".ToConsoleString(ConsoleColor.Green));
                        }
                    }
                    break;

                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}
