﻿using PowerArgs;

namespace RB.Stats.Db.Manage.Commands
{
    public abstract class CommandBase
    {
        [ArgShortcut("c")]
        [ArgShortcut(ArgShortcutPolicy.ShortcutsOnly)]
        [ArgDescription("DB connection string.")]
        
        public string ConnectionString { get; set; }

        [ArgShortcut("p")]
        [ArgShortcut(ArgShortcutPolicy.ShortcutsOnly)]
        [ArgDescription("DB provider type.")]        
        public DatabaseProviderType ProviderType { get; set; }

        [ArgShortcut("t")]
        [ArgShortcut(ArgShortcutPolicy.ShortcutsOnly)]
        [ArgDescription("DB connection timeout.")]
        [DefaultValue(60)]
        public int Timeout { get; set; }
    }
}
