﻿using PowerArgs;
using RB.Stats.Db.Manage.Commands;

namespace RB.Stats.Db.Manage
{
    [ArgDescription("RTS Board Statistics Database tools")]    
    public sealed class ProgramCommands
    {
        [ArgRequired]
        [ArgPosition(0)]
        public string Action { get; set; }

        #region create

        [ArgShortcut("create")]
        [ArgShortcut(ArgShortcutPolicy.ShortcutsOnly)]
        [ArgDescription("Create new empty database")]
        public CreateDatabaseCommand CreateArgs { get; set; }
                
        public static void Create(CreateDatabaseCommand command)
        {
            command.Execute();
        }

        #endregion
        
        #region upgrade

        [ArgShortcut("upgrade")]
        [ArgShortcut(ArgShortcutPolicy.ShortcutsOnly)]
        [ArgDescription("Perform database schema update")]
        public UpgradeCommand UpdateArgs { get; set; }

        public static void Update(UpgradeCommand command)
        {
            command.Execute();
        }

        #endregion

        #region downgrade

        [ArgShortcut("downgrade")]
        [ArgShortcut(ArgShortcutPolicy.ShortcutsOnly)]
        [ArgDescription("Perform database schema downgrade")]
        public DowngradeCommand DowngradeArgs { get; set; }

        public static void Downgrade(DowngradeCommand command)
        {
            command.Execute();
        }

        #endregion

        #region create-user

        [ArgShortcut("create-user")]
        [ArgShortcut(ArgShortcutPolicy.ShortcutsOnly)]
        [ArgDescription("Create user for database")]
        public CreateUserCommand CreateDatabaseUserArgs { get; set; }

        public static void CreateDatabaseUser(CreateUserCommand command)
        {
            command.Execute();
        }

        #endregion
       
    }
}
