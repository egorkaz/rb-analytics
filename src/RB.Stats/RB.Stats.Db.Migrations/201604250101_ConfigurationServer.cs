﻿using RB.Stats.Db;

namespace RB.Stats.Db.Migrations
{
    [MigrationAttribute(year: 2016, month: 4, day: 25, hour: 1, minute: 1, transactionBehavior: FluentMigrator.TransactionBehavior.None)]
    public class _201604250101_ConfigurationServer : FluentMigrator.Migration
    {
        /// <summary>
        ///     Накатить миграцию
        /// </summary>
        public override void Up()
        {
            var databaseName = (ApplicationContext as ApplicationContext).DatabaseName;

            Execute.Sql($"ALTER DATABASE [{databaseName}] SET SINGLE_USER WITH ROLLBACK IMMEDIATE"); // однопользовательский режим указывает, что одновременный доступ к базе данных получает только один пользователь
            Execute.Sql($"ALTER DATABASE [{databaseName}] SET ALLOW_SNAPSHOT_ISOLATION ON");
            Execute.Sql($"ALTER DATABASE [{databaseName}] SET READ_COMMITTED_SNAPSHOT ON");
            Execute.Sql($"ALTER DATABASE [{databaseName}] SET MULTI_USER WITH ROLLBACK IMMEDIATE"); // многопользовательский режим
        }

        /// <summary>
        ///     Откатить миграцию
        /// </summary>
        public override void Down() { }
    }
}
