﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RB.Stats.Db.Migrations
{
    [MigrationAttribute(year: 2016, month: 4, day: 25, hour: 2, minute: 2, transactionBehavior: FluentMigrator.TransactionBehavior.None)]
    public class _201604250202_InitDatabase : FluentMigrator.Migration
    {
        /// <summary>
        ///     Накатить миграцию
        /// </summary>
        public override void Up()
        {
            #region Create table: Parties
            {
                var table = new TableMetadata<Objects.Party>();
                if (!Schema.Table(table.Name).Exists())
                {
                    var t = Create.Table(table.Name);
                    t.WithColumn(table.Column(_ => _.Id)).AsInt32().NotNullable().PrimaryKey().Identity();

                    var partyCode = table.Column(_ => _.PartyCode);
                    MigrationCreateColumn.Create(t, table.Name, table.Column(partyCode));
                    var shortNameRu = table.Column(_ => _.ShortNameRu);
                    MigrationCreateColumn.Create(t, table.Name, table.Column(shortNameRu));
                    var shortNameEn = table.Column(_ => _.ShortNameEn);
                    MigrationCreateColumn.Create(t, table.Name, table.Column(shortNameEn));
                    var fullNameRu = table.Column(_ => _.FullNameRu);
                    MigrationCreateColumn.Create(t, table.Name, table.Column(fullNameRu));
                    var fullNameEn = table.Column(_ => _.FullNameEn);
                    MigrationCreateColumn.Create(t, table.Name, table.Column(fullNameEn));
                    var postalAddress = table.Column(_ => _.PostalAddress);
                    MigrationCreateColumn.Create(t, table.Name, table.Column(postalAddress));
                    var legalAddress = table.Column(_ => _.LegalAddress);
                    MigrationCreateColumn.Create(t, table.Name, table.Column(legalAddress));
                    var email = table.Column(_ => _.Email);
                    MigrationCreateColumn.Create(t, table.Name, table.Column(email));
                    var phone = table.Column(_ => _.Phone);
                    MigrationCreateColumn.Create(t, table.Name, table.Column(phone));
                    var country = table.Column(_ => _.Country);
                    MigrationCreateColumn.Create(t, table.Name, table.Column(country));
                    var jurisdiction = table.Column(_ => _.Jurisdiction);
                    MigrationCreateColumn.Create(t, table.Name, table.Column(jurisdiction));
                    var organizationType = table.Column(_ => _.OrganizationType);
                    MigrationCreateColumn.Create(t, table.Name, table.Column(organizationType));
                }
            }
            #endregion

            #region Create table: MarketSectors
            {
                var table = new TableMetadata<Objects.MarketSector>();
                if (!Schema.Table(table.Name).Exists())
                {
                    var t = Create.Table(table.Name);
                    t.WithColumn(table.Column(_ => _.Id)).AsInt32().NotNullable().PrimaryKey().Identity();

                    var sectorCode = table.Column(_ => _.SectorCode);
                    MigrationCreateColumn.Create(t, table.Name, table.Column(sectorCode));
                    var nameRu = table.Column(_ => _.NameRu);
                    MigrationCreateColumn.Create(t, table.Name, table.Column(nameRu));                 
                    var nameEn = table.Column(_ => _.NameEn);
                    MigrationCreateColumn.Create(t, table.Name, table.Column(nameEn));
                    var isActive = table.Column(_ => _.IsActive);
                    MigrationCreateColumn.Create(t, table.Name, table.Column(isActive));
                }
            }
            #endregion

            #region Create table: InstrumentClasses
            {
                var table = new TableMetadata<Objects.InstrumentClass>();
                if (!Schema.Table(table.Name).Exists())
                {
                    var t = Create.Table(table.Name);
                    t.WithColumn(table.Column(_ => _.Id)).AsInt32().NotNullable().PrimaryKey().Identity();

                    var code = table.Column(_ => _.ClassCode);
                    MigrationCreateColumn.Create(t, table.Name, table.Column(code));

                    var nameRu = table.Column(_ => _.NameRu);
                    MigrationCreateColumn.Create(t, table.Name, table.Column(nameRu));

                    var nameEn = table.Column(_ => _.NameEn);
                    MigrationCreateColumn.Create(t, table.Name, table.Column(nameEn));

                    var marketSectorId = table.Column(_ => _.MarketSectorId);
                    MigrationCreateColumn.Create(t, table.Name, table.Column(marketSectorId));

                    var isActive = table.Column(_ => _.IsActive);
                    MigrationCreateColumn.Create(t, table.Name, table.Column(isActive));
                }
            }
            #endregion

            #region Create table: Coins
            {
                var table = new TableMetadata<Objects.Coin>();
                if (!Schema.Table(table.Name).Exists())
                {
                    var t = Create.Table(table.Name);
                    t.WithColumn(table.Column(_ => _.Id)).AsInt32().NotNullable().PrimaryKey().Identity();

                    var code = table.Column(_ => _.CoinCode);
                    MigrationCreateColumn.Create(t, table.Name, table.Column(code));

                    var nameRu = table.Column(_ => _.NameRu);
                    MigrationCreateColumn.Create(t, table.Name, table.Column(nameRu));

                    var nameEn = table.Column(_ => _.NameEn);
                    MigrationCreateColumn.Create(t, table.Name, table.Column(nameEn));

                    var catalogueNumber = table.Column(_ => _.CatalogueNumber);
                    MigrationCreateColumn.Create(t, table.Name, table.Column(catalogueNumber));

                    var series = table.Column(_ => _.Series);
                    MigrationCreateColumn.Create(t, table.Name, table.Column(series));

                    var nominalValue = table.Column(_ => _.NominalValue);
                    MigrationCreateColumn.Create(t, table.Name, table.Column(nominalValue));

                    var nominalCurrency = table.Column(_ => _.NominalCurrency);
                    MigrationCreateColumn.Create(t, table.Name, table.Column(nominalCurrency));

                    var quality = table.Column(_ => _.Quality);
                    MigrationCreateColumn.Create(t, table.Name, table.Column(quality));

                    var metalType = table.Column(_ => _.MetalType);
                    MigrationCreateColumn.Create(t, table.Name, table.Column(metalType));

                    var metalAssay = table.Column(_ => _.MetalAssay);
                    MigrationCreateColumn.Create(t, table.Name, table.Column(metalAssay));

                    var chemicallyPureMetalContent = table.Column(_ => _.ChemicallyPureMetalContent);
                    MigrationCreateColumn.Create(t, table.Name, table.Column(chemicallyPureMetalContent));

                    var weight = table.Column(_ => _.Weight);
                    MigrationCreateColumn.Create(t, table.Name, table.Column(weight));

                    var diameter = table.Column(_ => _.Diameter);
                    MigrationCreateColumn.Create(t, table.Name, table.Column(diameter));

                    var thickness = table.Column(_ => _.Thickness);
                    MigrationCreateColumn.Create(t, table.Name, table.Column(thickness));

                    var issuingVolumes = table.Column(_ => _.IssuingVolumes);
                    MigrationCreateColumn.Create(t, table.Name, table.Column(issuingVolumes));

                    var issueDate = table.Column(_ => _.IssueDate);
                    MigrationCreateColumn.Create(t, table.Name, table.Column(issueDate));

                    var mint = table.Column(_ => _.Mint);
                    MigrationCreateColumn.Create(t, table.Name, table.Column(mint));

                    var additionalInformation = table.Column(_ => _.AdditionalInformation);
                    MigrationCreateColumn.Create(t, table.Name, table.Column(additionalInformation));

                    var cbrUrl = table.Column(_ => _.CbrUrl);
                    MigrationCreateColumn.Create(t, table.Name, table.Column(cbrUrl));

                    var instrumentClassId = table.Column(_ => _.InstrumentClassId);
                    MigrationCreateColumn.Create(t, table.Name, table.Column(instrumentClassId));

                    var isActive = table.Column(_ => _.IsActive);
                    MigrationCreateColumn.Create(t, table.Name, table.Column(isActive));
                }
            }
            #endregion

            #region Create table: Equities
            {
                var table = new TableMetadata<Objects.Equity>();
                if (!Schema.Table(table.Name).Exists())
                {
                    var t = Create.Table(table.Name);
                    t.WithColumn(table.Column(_ => _.Id)).AsInt32().NotNullable().PrimaryKey().Identity();

                    var code = table.Column(_ => _.EquityCode);
                    MigrationCreateColumn.Create(t, table.Name, table.Column(code));

                    var nameRu = table.Column(_ => _.NameRu);
                    MigrationCreateColumn.Create(t, table.Name, table.Column(nameRu));

                    var nameEn = table.Column(_ => _.NameEn);
                    MigrationCreateColumn.Create(t, table.Name, table.Column(nameEn));

                    var isin = table.Column(_ => _.Isin);
                    MigrationCreateColumn.Create(t, table.Name, table.Column(isin));

                    var cfi = table.Column(_ => _.Cfi);
                    MigrationCreateColumn.Create(t, table.Name, table.Column(cfi));

                    var regnum = table.Column(_ => _.Regnum);
                    MigrationCreateColumn.Create(t, table.Name, table.Column(regnum));

                    var unitNominal = table.Column(_ => _.UnitNominal);
                    MigrationCreateColumn.Create(t, table.Name, table.Column(unitNominal));

                    var currency = table.Column(_ => _.Currency);
                    MigrationCreateColumn.Create(t, table.Name, table.Column(currency));

                    var issuingVolumes = table.Column(_ => _.IssuingVolumes);
                    MigrationCreateColumn.Create(t, table.Name, table.Column(issuingVolumes));
                    
                    var instrumentClassId = table.Column(_ => _.InstrumentClassId);
                    MigrationCreateColumn.Create(t, table.Name, table.Column(instrumentClassId));

                    var isActive = table.Column(_ => _.IsActive);
                    MigrationCreateColumn.Create(t, table.Name, table.Column(isActive));
                }
            }
            #endregion

            #region Create table: UnsecuredLoans
            {
                var table = new TableMetadata<Objects.UnsecuredLoan>();
                if (!Schema.Table(table.Name).Exists())
                {
                    var t = Create.Table(table.Name);
                    t.WithColumn(table.Column(_ => _.Id)).AsInt32().NotNullable().PrimaryKey().Identity();

                    var unsecuredLoanCode = table.Column(_ => _.UnsecuredLoanCode);
                    MigrationCreateColumn.Create(t, table.Name, table.Column(unsecuredLoanCode));

                    var nameRu = table.Column(_ => _.NameRu);
                    MigrationCreateColumn.Create(t, table.Name, table.Column(nameRu));

                    var nameEn = table.Column(_ => _.NameEn);
                    MigrationCreateColumn.Create(t, table.Name, table.Column(nameEn));

                    var currency = table.Column(_ => _.Currency);
                    MigrationCreateColumn.Create(t, table.Name, table.Column(currency));
                    
                    var instrumentClassId = table.Column(_ => _.InstrumentClassId);
                    MigrationCreateColumn.Create(t, table.Name, table.Column(instrumentClassId));

                    var isActive = table.Column(_ => _.IsActive);
                    MigrationCreateColumn.Create(t, table.Name, table.Column(isActive));
                }
            }
            #endregion

            #region Create table: QuoteLogs
            {
                var table = new TableMetadata<Objects.QuoteLog>();
                if (!Schema.Table(table.Name).Exists())
                {
                    var t = Create.Table(table.Name);
                    t.WithColumn(table.Column(_ => _.Id)).AsInt64().NotNullable().PrimaryKey().Identity();

                    var InstrumnetId = table.Column(_ => _.InstrumnetId);
                    MigrationCreateColumn.Create(t, table.Name, table.Column(InstrumnetId));

                    var PartyId = table.Column(_ => _.PartyId);
                    MigrationCreateColumn.Create(t, table.Name, table.Column(PartyId));

                    var QuoteId = table.Column(_ => _.QuoteId);
                    MigrationCreateColumn.Create(t, table.Name, table.Column(QuoteId));

                    var ActiveDateTime = table.Column(_ => _.ActiveDateTime);
                    MigrationCreateColumn.Create(t, table.Name, table.Column(ActiveDateTime));

                    var ActiveRevisionId = table.Column(_ => _.ActiveRevisionId);
                    MigrationCreateColumn.Create(t, table.Name, table.Column(ActiveRevisionId));

                    var CancelledDateTime = table.Column(_ => _.CancelledDateTime);
                    MigrationCreateColumn.Create(t, table.Name, table.Column(CancelledDateTime));

                    var CancelledRevisionId = table.Column(_ => _.CancelledRevisionId);
                    MigrationCreateColumn.Create(t, table.Name, table.Column(CancelledRevisionId));

                    var Direction = table.Column(_ => _.Direction);
                    MigrationCreateColumn.Create(t, table.Name, table.Column(Direction));

                    var Price = table.Column(_ => _.Price);
                    MigrationCreateColumn.Create(t, table.Name, table.Column(Price));

                    var PriceCurrency = table.Column(_ => _.PriceCurrency);
                    MigrationCreateColumn.Create(t, table.Name, table.Column(PriceCurrency));

                    var Quantity = table.Column(_ => _.Quantity);
                    MigrationCreateColumn.Create(t, table.Name, table.Column(Quantity));
                }
            }
            #endregion

            #region Create table: MarketTrades
            {
                var table = new TableMetadata<Objects.MarketTrade>();
                if (!Schema.Table(table.Name).Exists())
                {
                    var t = Create.Table(table.Name);
                    t.WithColumn(table.Column(_ => _.Id)).AsInt64().NotNullable().PrimaryKey().Identity();

                    var MarketTradeId = table.Column(_ => _.MarketTradeId);
                    MigrationCreateColumn.Create(t, table.Name, table.Column(MarketTradeId));

                    var InstrumnetId = table.Column(_ => _.InstrumnetId);
                    MigrationCreateColumn.Create(t, table.Name, table.Column(InstrumnetId));
                    
                    var DateTime = table.Column(_ => _.DateTime);
                    MigrationCreateColumn.Create(t, table.Name, table.Column(DateTime));

                    var DescriptionRu = table.Column(_ => _.DescriptionRu);
                    MigrationCreateColumn.Create(t, table.Name, table.Column(DescriptionRu));

                    var DescriptionEn = table.Column(_ => _.DescriptionEn);
                    MigrationCreateColumn.Create(t, table.Name, table.Column(DescriptionEn));

                    var Price = table.Column(_ => _.Price);
                    MigrationCreateColumn.Create(t, table.Name, table.Column(Price));

                    var PriceCurrency = table.Column(_ => _.PriceCurrency);
                    MigrationCreateColumn.Create(t, table.Name, table.Column(PriceCurrency));

                    var Quantity = table.Column(_ => _.Quantity);
                    MigrationCreateColumn.Create(t, table.Name, table.Column(Quantity));
                }
            }
            #endregion
        }

        /// <summary>
        ///     Откатить миграцию
        /// </summary>
        public override void Down() { }
    }
}
