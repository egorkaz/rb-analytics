﻿
namespace RB.Stats.Db.Migrations
{
    /// <summary>
    /// Mark all migrations with this INSTEAD of [Migration].
    /// </summary>
    public class MigrationAttribute : FluentMigrator.MigrationAttribute
    {

        public MigrationAttribute(int year, int month, int day, int hour, int minute, FluentMigrator.TransactionBehavior transactionBehavior = FluentMigrator.TransactionBehavior.Default)
           : base(CalculateValue(year, month, day, hour, minute), transactionBehavior)
        { }


        public MigrationAttribute(int year, int month, int day, int hour, int minute, string author, FluentMigrator.TransactionBehavior transactionBehavior = FluentMigrator.TransactionBehavior.Default)
           : base(CalculateValue(year, month, day, hour, minute), transactionBehavior)
        {
            this.Author = author;
        }

        public string Author { get; private set; }

        private static long CalculateValue(int year, int month, int day, int hour, int minute)
        {
            return year * 100000000L + month * 1000000L + day * 10000L + hour * 100L + minute;
        }
    }
}
