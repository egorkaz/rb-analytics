﻿using LinqToDB;

namespace RB.Stats.Db.Migrations
{
    public static class MigrationCreateColumn
    {
        /// <summary>
        /// Создает столбец для заданной таблицы или вызывает exception
        /// </summary>
        /// <param name="table"></param>
        /// <param name="tableName"></param>
        /// <param name="column"></param>
        public static void Create(FluentMigrator.Builders.Create.Table.ICreateTableWithColumnOrSchemaOrDescriptionSyntax table, string tableName, ColumnMetaData column)
        {
            switch (column.TypeName)
            {
                case "Int32":
                    if (column.IsNullable)
                        table.WithColumn(column.Name).AsInt32().Nullable();
                    else
                        table.WithColumn(column.Name).AsInt32().NotNullable();
                    break;

                case "Boolean":
                    if (column.IsNullable)
                        table.WithColumn(column.Name).AsBoolean().Nullable();
                    else
                        table.WithColumn(column.Name).AsBoolean().NotNullable();
                    break;

                case "Int64":
                    if (column.IsNullable)
                        table.WithColumn(column.Name).AsInt64().Nullable();
                    else
                        table.WithColumn(column.Name).AsInt64().NotNullable();
                    break;

                case "String":
                    if (column.IsNullable)
                        table.WithColumn(column.Name).AsString().Nullable();
                    else
                        table.WithColumn(column.Name).AsString().NotNullable();
                    break;

                case "DateTime":
                    if (column.IsNullable)
                        table.WithColumn(column.Name).AsDateTime().Nullable();
                    else
                        table.WithColumn(column.Name).AsDateTime().NotNullable();
                    break;

                case "NVarChar":
                    if (column.IsNullable)
                        table.WithColumn(column.Name).AsString().Nullable();
                    else
                        table.WithColumn(column.Name).AsString().NotNullable();
                    break;

                case "VarChar":
                    if (column.IsNullable)
                        table.WithColumn(column.Name).AsString().Nullable();
                    else
                        table.WithColumn(column.Name).AsString().NotNullable();
                    break;

                case "NChar":
                    if (column.IsNullable)
                        table.WithColumn(column.Name).AsFixedLengthString(column.Length).Nullable();
                    else
                        table.WithColumn(column.Name).AsFixedLengthString(column.Length).NotNullable();
                    break;

                case "NText":
                    if (column.IsNullable)
                        table.WithColumn(column.Name).AsCustom("NText").Nullable();
                    //table.WithColumn(column.Name).AsString().Nullable();
                    else
                        //table.WithColumn(column.Name).AsString().NotNullable();
                    table.WithColumn(column.Name).AsXml().NotNullable();
                    break;

                case "Image":
                    if (column.IsNullable)
                        table.WithColumn(column.Name).AsCustom("image").Nullable();
                    else
                        table.WithColumn(column.Name).AsCustom("image").NotNullable();
                    break;

                case "Byte[]":
                    if (column.IsNullable)
                        table.WithColumn(column.Name).AsCustom("varbinary(max)").Nullable();
                    else
                        table.WithColumn(column.Name).AsCustom("varbinary(max)").NotNullable();
                    break;

                case "Decimal":
                    if (column.IsNullable)
                        table.WithColumn(column.Name).AsDecimal(column.Length, column.Precision).Nullable();
                    else
                        table.WithColumn(column.Name).AsDecimal(column.Length, column.Precision).NotNullable();
                    break;

                default:
                    throw new System.ArgumentException($"Table [{tableName}].[{column.Name}]. Column type ({column.TypeName}) is no defined");

            }
        }

    }
}
