﻿using System;

using System.Collections.Generic;

using System.Linq.Expressions;
using System.Reflection;
using LinqToDB.Mapping;

namespace RB.Stats.Db.Migrations
{
    /// <summary>
    ///     Метаданные таблицы
    /// </summary>
    public interface ITableMetadata
    {
        /// <summary>
        ///     Название таблицы
        /// </summary>
        string Name { get; }
    }

    /// <summary>
    /// Информация о таблице базы данных
    /// </summary>
    public sealed class TableMetadata<T> : ITableMetadata
    {
        /// <summary>
        ///     Название таблицы
        /// </summary>
        public string Name => typeof(T).GetCustomAttribute<TableAttribute>().Name;

        /// <summary>
        ///     Название колонки таблицы
        /// </summary>
        public string Column<TColumn>(Expression<Func<T, TColumn>> selector)
        {
            var expression = selector.Body;

            var member = ((MemberExpression)expression).Member;

            var columnAttribute = member.GetCustomAttribute<ColumnAttribute>();
            if (columnAttribute != null && !string.IsNullOrEmpty(columnAttribute.Name))
            {
                return columnAttribute.Name;
            }

            return member.Name;
        }

        /// <summary>
        /// Возвращает заданную колонку
        /// </summary>       
        /// <returns></returns>
        public ColumnMetaData Column(string columnName)
        {
            var p = typeof(T).GetProperty(columnName);

            var attr = p.GetCustomAttribute<ColumnAttribute>();
            if (attr == null || attr.IsPrimaryKey) return null;

            var c = new ColumnMetaData();
            c.Name = attr.Name;
            c.TypeName = p.PropertyType.Name;
            if (attr.DataType != LinqToDB.DataType.Undefined)
                c.TypeName = attr.DataType.ToString();
            c.IsNullable = attr.CanBeNull;
            c.Length = attr.Length;
            c.Precision = attr.Precision;

            return c;
        }

        /// <summary>
        /// Возвращает список всех колонок, без Primary key
        /// </summary>       
        /// <returns></returns>
        public IList<ColumnMetaData> Columns()
        {
            List<ColumnMetaData> columns = new List<ColumnMetaData>();
            foreach (var p in typeof(T).GetProperties())
            {

                var attr = p.GetCustomAttribute<ColumnAttribute>();
                if (attr == null || attr.IsPrimaryKey) continue;

                var c = new ColumnMetaData();
                c.Name = attr.Name;
                c.TypeName = p.PropertyType.Name;
                if (attr.DataType != LinqToDB.DataType.Undefined)
                    c.TypeName = attr.DataType.ToString();
                c.IsNullable = attr.CanBeNull;
                c.Length = attr.Length;
                c.Precision = attr.Precision;
               

                columns.Add(c);
            }

            return columns;
        }        
    }

    public class ColumnMetaData
    {
        public string Name { get; set; }
        public string TypeName { get; set; }
        public bool IsNullable { get; set; }
        public int Length { get; set; }
        public int Precision { get; set; }
    }
}
