﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNet.SignalR.Client;
using Microsoft.AspNet.SignalR.Client.Http;
using Microsoft.AspNet.SignalR.Client.Transports;
using System.Net.Http;
using NLog;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace RB.Stats.Data.Receiver
{
    public class DynamicDataFeed
    {
        #region consts

        private const string AddEventName = "add";
        private const string UpdateEventName = "update";
        private const string RemoveEventName = "remove";
        private const string EndOfStreamEventName = "eos";

        private const string SubscribeMethodName = "subscribe";
        private const string UpdateMethodName = "update";
        private const string UnsubscribeMethodName = "unsubscribe";

        #endregion

        public Common.ReceiverState ReceiverState { get; private set; } = Common.ReceiverState.Stoped;

        private Dictionary<string, DataFeedItem> addedDataContainer = new Dictionary<string, DataFeedItem>();
        private Dictionary<string, DataFeedItem> updatedDataContainer = new Dictionary<string, DataFeedItem>();
        private Dictionary<string, DataFeedItem> removedDataContainer = new Dictionary<string, DataFeedItem>();

        NLog.Logger _logger;
        HubConnection _hubConnection;
        IHubProxy _hubProxy;

        string _feedName;
        Common.AuthClient _authClient;
        Common.HttpClientWrapper _httpClient;
        Common.Subscription _subscription;
                
        public DynamicDataFeed(string uri, string feedName, Common.AuthClient authClient)
        {
            _feedName = feedName;

            _hubConnection = new HubConnection($"{uri}/api/feed");
            _hubConnection.StateChanged += HubConnection_StateChanged;
            _hubConnection.Reconnecting += HubConnection_Reconnecting;
            _hubConnection.Reconnected += HubConnection_Reconnected;
            _hubConnection.Error += HubConnection_Error; ;

            _hubProxy = _hubConnection.CreateHubProxy(feedName);
            _hubProxy.On<JObject>(AddEventName, args => AddEventHandler(args));
            _hubProxy.On<JObject>(UpdateEventName, args => UpdateEventHandler(args));
            _hubProxy.On<JObject>(RemoveEventName, args => RemoveEventHandler(args));
            _hubProxy.On<JObject>(EndOfStreamEventName, args => EndOfStreamEventHandler(args));

            _authClient = authClient;//= new Common.AuthClient(uri, userGrantType, username, password);
            _httpClient = new Common.HttpClientWrapper(new DefaultHttpClient());                   
         
            _logger = LogManager.GetLogger($"DataFeed ({feedName})");
        }

        public List<DataFeedItem> GetAddedDataContainer()
        {
            return addedDataContainer.Values.ToList();
        }

        #region HubConnection

        private void HubConnection_StateChanged(StateChange state)
        {
            if (state.NewState == ConnectionState.Connected)
            {
                // Передергиваем подписки, если таковые имелись
                //_authClient.RefreshToken();
                //_httpClient.Token = _authClient.Tokens.AccessToken;               
            }
            _logger.Info($"HubConnection has a new state {state.NewState}");
        }

        private void HubConnection_Reconnecting() { }

        private void HubConnection_Reconnected()
        {
            UnSubscribe();
            Connect();
            Subscribe();
        }

        private void HubConnection_Error(Exception obj)
        {
            _hubConnection.Stop();
            _logger.Error($"HubConnection_Error. Message: {obj.Message}. StackTrace: {obj.StackTrace}. InnerException: {obj.InnerException}");
        }

        public void Connect()
        {
            addedDataContainer.Clear();
            updatedDataContainer.Clear();
            removedDataContainer.Clear();

            var tokens = _authClient.GetTokens();
            _httpClient.Token = tokens.AccessToken;
        }

        public async Task StartAsync()
        {
            await _hubConnection.Start(_httpClient).ContinueWith(task => {
                if (task.IsFaulted)
                {
                    _logger.Error($"There was an error opening the connection:{task.Exception.GetBaseException()}.");

                    throw new Exception($"There was an error opening the connection:{task.Exception.GetBaseException()}.");
                }
                else {
                    ReceiverState = Common.ReceiverState.Run;
                    _logger.Info("Connected.");
                }

            });
        }

        public void Stop()
        {            
            _hubConnection.Stop();
            ReceiverState = Common.ReceiverState.Stoped;
        }

        #endregion

        #region Subscription methods

        public void Subscribe(string ticker, JObject filter)
        {
            if (_subscription == null)
            {
                _subscription = new Common.Subscription(ticker, filter);
                InvokeMethod(SubscribeMethodName, new object[] { ticker, filter });
            }
        }

        public void Subscribe()
        {
            if (_subscription != null)
            {
                InvokeMethod(SubscribeMethodName, new object[] { _subscription.Ticker, _subscription.Filter });
            }
        }

        public void UpdateSubscribe(JObject filter)
        {
            if (_subscription != null)
            {
                _subscription.SetFilter(filter);
                InvokeMethod(UpdateMethodName, new object[] { _subscription.Ticker, filter });
            }
        }

        public void UnSubscribe()
        {
            if (_subscription != null)
            {
                InvokeMethod(UnsubscribeMethodName, new object[] { _subscription.Ticker });
            }
        }

        private void InvokeMethod(string method, params object[] args)
        {
            _hubProxy.Invoke<Common.ErrorContract>(method, args).ContinueWith(task =>
            {
                if (task.IsFaulted)
                {
                    Console.WriteLine($"There was an error calling subscribe: {task.Exception.GetBaseException()}");
                }
                else {
                    var result = task.Result;

                    if(!result.Success)
                    {
                        _logger.Error($"InvokeMethod '{_feedName}::{method}()' failed: {result.ErrorCode} {result.Message}");
                        throw new Exception(result.ToFriendlyString());                        
                    }
                    else
                    {
                        _logger.Info($"InvokeMethod '{_feedName}::{method}()' success");
                    }                    
                }
            });

        }

        #endregion

        #region EventHandler methods
        private void AddEventHandler(JObject arg)
        {
            string id = (string)arg["id"];
            addedDataContainer.Add(id, new DataFeedItem { DateTime = DateTime.Now, Item = arg });
        }

        private void UpdateEventHandler(JObject arg)
        {
            string id = (string)arg["id"];
            updatedDataContainer.Add(id, new DataFeedItem { DateTime = DateTime.Now, Item = arg });
        }

        private void RemoveEventHandler(JObject arg)
        {
            string id = (string)arg["id"];
            updatedDataContainer.Add(id, new DataFeedItem { DateTime = DateTime.Now, Item = arg });
        }

        private void EndOfStreamEventHandler(JObject arg)
        {
            _logger.Info($"Feed:{_feedName}. {arg}");
        }
        #endregion
    }

    public class DataFeedItem
    {
        public DateTime DateTime { get; set; }
        public JObject Item { get; set; }
    }
}
