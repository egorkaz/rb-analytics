﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace RB.Stats.Data.Receiver
{
    public class StaticDataFeed
    {
        Common.AuthClient _authClient;

        public StaticDataFeed(Common.AuthClient authClient)
        {
            _authClient = authClient;
        }
        
        /// <summary>
        /// Возвращает список всех секторов рынка
        /// </summary>
        /// <returns></returns>
        public async Task<JObject> GetMarketSectorsAsync()
        {
            JObject filter = new JObject();
            filter["includeInactive"] = true;

            var resp = await _authClient.PostAsync("api/marketsector/list", filter);
                        
            return resp;
        }

        /// <summary>
        /// Возвращает список всех классов инструментов
        /// </summary>
        /// <returns></returns>
        public async Task<JObject> GetInstrumentClasses()
        {
            JObject filter = new JObject();
            filter["includeInactive"] = true;
            filter["marketSectorId"] = null;

            var resp = await _authClient.PostAsync("api/instrumentclass/list", filter);

            return resp;
        }

        /// <summary>
        /// Возвращает список всех инструментов: ценных бумаг, монет, кредитов
        /// </summary>
        /// <returns></returns>
        public async Task<JObject> GetInstruments()
        {
            JObject filter = new JObject();
            filter["includeInactive"] = true;

            var resp = await _authClient.PostAsync("api/instrument/list", filter);

            return resp;
        }

        /// <summary>
        /// Запрашивает валютные курсы. Возможно за конкретную дату
        /// </summary>
        /// <returns></returns>
        public async Task<JObject> GetCurrencyRates(string currencyCode, DateTime date)
        {
            JObject filter = new JObject();
            filter["currency"] = currencyCode;
            filter["date"] = date;

            var resp = await _authClient.PostAsync("api/currencyquote/get", filter);
            
            return resp;
        }

        /// <summary>
        /// Запрашивает ревизии котировки
        /// </summary>
        /// <returns></returns>
        public async Task<JObject> GetQuoteHistory(string quoteId)
        {
            JObject filter = new JObject();
            filter["id"] = quoteId;

            var resp = await _authClient.PostAsync("api/quote/history", filter);
            
            return resp;
        }

        /// <summary>
        /// Запрашивает конкретную ревизию для указанной котировки
        /// </summary>
        /// <returns></returns>
        public async Task<JObject> GetQuoteRevision(string quoteId, int revision)
        {
            JObject filter = new JObject();
            filter["id"] = quoteId;
            filter["revision"] = revision;
            filter["includeContent"] = "NONE";

            var resp = await _authClient.PostAsync("api/quote/get", filter);

            return resp;
        }
    }
}
