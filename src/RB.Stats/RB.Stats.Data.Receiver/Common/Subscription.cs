﻿using Newtonsoft.Json.Linq;

namespace RB.Stats.Data.Receiver.Common
{
    internal class Subscription
    {
        public string Ticker { get; private set; }
        public JObject Filter { get; private set; }

        public Subscription(string ticker, JObject filter)
        {
            Ticker = ticker;
            Filter = filter;
        }

        public void SetFilter(JObject filter)
        {
            Filter = filter;
        }
    }
}
