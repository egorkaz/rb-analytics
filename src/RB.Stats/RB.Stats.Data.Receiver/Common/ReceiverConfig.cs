﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RB.Stats.Data.Receiver.Common
{
    public class ReceiverConfig
    {
        public string MainUri { get; set; }
        public CommonWebApiConfig WebApiConfig { get; set; }
        public DataFeedConfig AllQuotesFeed { get; set; }
        public DataFeedConfig PublicQuotesFeed { get; set; }
        public DataFeedConfig MarketTradesFeed { get; set; }
    }

    public class DataFeedConfig : CommonWebApiConfig
    {
        public string FeedHubName { get; set; }
    }

    public class CommonWebApiConfig
    {        
        public string GrantType { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
