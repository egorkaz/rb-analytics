﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RB.Stats.Data.Receiver.Common
{
    public enum ReceiverState
    {
        /// <summary>
        /// Запущен
        /// </summary>
        Run,

        /// <summary>
        /// Остановлен
        /// </summary>
        Stoped
    }
}
