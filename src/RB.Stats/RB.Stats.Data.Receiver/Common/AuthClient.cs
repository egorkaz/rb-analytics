﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace RB.Stats.Data.Receiver.Common
{
    public class AuthClient
    {
        NLog.Logger _logger;
        string _uri;
        string _grantType;
        string _username;
        string _password;

        public Tokens Tokens { get; private set; }

        public AuthClient(string uri, string grantType, string username, string password)
        {
            _logger = NLog.LogManager.GetLogger($"AuthClient [{grantType}][{username}]");
            _uri = uri;
            _grantType = grantType;
            _username = username;
            _password = password;

            Tokens = new Tokens();
        }

        /// <summary>
        /// Аутентификация (Получение токена) /api/token/get
        /// </summary>
        /// <param name="uri"></param>
        /// <param name="grantType"></param>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public Tokens GetTokens()
        {
            using (var client = new HttpClient())
            {
                Newtonsoft.Json.Linq.JObject auth = new Newtonsoft.Json.Linq.JObject();
                auth["grant_type"] = _grantType; 
                auth["username"] = _username;
                auth["password"] = _password;
                
                var response = client.PostAsync(_uri + "/api/token/get",
                    new StringContent(auth.ToString(),
                        Encoding.UTF8, "application/json"))
                        .Result;
                
                if (response.IsSuccessStatusCode)
                {
                    Newtonsoft.Json.Linq.JObject content = (Newtonsoft.Json.Linq.JObject)JsonConvert.DeserializeObject(
                        response.Content.ReadAsStringAsync()
                        .Result);

                    Tokens.AccessToken = (string)content["access_token"];
                    Tokens.RefreshToken = (string)content["refresh_token"];
                    
                    return this.Tokens;
                }
                else
                {
                    throw new Exception($"Getting access token user:'{_username}', password:'{_password}', granttype: '{_grantType}' is failed. {response.Content?.ReadAsStringAsync()?.Result}");
                }
            }            
        }

        /// <summary>
        /// Аутентификация (Обновление токена) /api/token/refresh
        /// </summary>
        /// <param name="uri"></param>
        /// <param name="refresh_token"></param>
        /// <returns></returns>
        public void RefreshToken()
        {
            if (_grantType == "password")
            {
                using (var client = new HttpClient())
                {
                    Newtonsoft.Json.Linq.JObject auth = new Newtonsoft.Json.Linq.JObject();
                    auth["refresh_token"] = this.Tokens.RefreshToken;

                    var response = client.PostAsync(_uri + "/api/token/refresh",
                        new StringContent(auth.ToString(),
                            Encoding.UTF8, "application/json"))
                            .Result;

                    if (response.IsSuccessStatusCode)
                    {
                        Newtonsoft.Json.Linq.JObject content = (Newtonsoft.Json.Linq.JObject)JsonConvert.DeserializeObject(
                            response.Content.ReadAsStringAsync()
                            .Result);

                        Tokens.AccessToken = (string)content["access_token"];
                        Tokens.RefreshToken = (string)content["refresh_token"];
                    }
                    else
                    {
                        throw new Exception($"Refresh token {this.Tokens.RefreshToken} is failed. {response.Content?.ReadAsStringAsync()?.Result}");
                    }
                }
            }
        }

        /// <summary>
        /// Вызов метода API. При неуспешном вызове возвращается null.
        /// </summary>
        /// <param name="apiUri"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        public async Task<JObject> PostAsync(string apiUri, JObject filter)
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", Tokens.AccessToken);
                var response = await client.PostAsync($"{_uri}/{apiUri}", new StringContent(filter.ToString(), Encoding.UTF8, "application/json"));

                if (response.IsSuccessStatusCode)
                {
                    var stringContent = await response.Content.ReadAsStringAsync();
                    JObject content = (JObject)JsonConvert.DeserializeObject(stringContent);

                    return content;
                }
                else
                {                    
                    _logger.Error($"Request {apiUri} faild. StatusCode {response.StatusCode}. ReasonPhrase: {response.ReasonPhrase}.");
                                     
                    return null;
                }
            }
        }


    }

    public class Tokens
    {
        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }
    }
}
