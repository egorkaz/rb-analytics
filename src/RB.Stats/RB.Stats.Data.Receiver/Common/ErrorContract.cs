﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace RB.Stats.Data.Receiver.Common
{
    /// <summary>
    ///     Контракт ошибки
    /// </summary>
    internal sealed class ErrorContract
    {
        /// <summary>
        ///     Результат операции - успех/неуспех
        /// </summary>
        [JsonProperty("success")]
        public bool Success { get; set; }

        /// <summary>
        ///     Идентификатор ошибки. Начинается с префикса "E_", например "E_NOT_AUTHORIZED", "E_NOT_FOUND", "E_INTERNAL_ERROR"
        /// </summary>
        [JsonProperty("error")]
        public string ErrorCode { get; set; }

        /// <summary>
        ///     Текстовое описание ошибки
        /// </summary>
        [JsonProperty("message")]
        public string Message { get; set; }

        /// <summary>
        ///     Подробности ошибки
        /// </summary>
        [JsonProperty("details")]
        public JToken Details { get; set; }

        public string ToFriendlyString()
        {
            if (Message != null)
            {
                return $"[{ErrorCode}] {Message}";
            }

            return $"[{ErrorCode}]";
        }        
    }
}
