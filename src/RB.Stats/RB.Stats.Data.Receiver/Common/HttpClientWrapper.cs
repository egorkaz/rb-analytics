﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.Specialized;
using System.Threading.Tasks;
using Microsoft.AspNet.SignalR.Client;
using Microsoft.AspNet.SignalR.Client.Http;

namespace RB.Stats.Data.Receiver.Common
{
    internal class HttpClientWrapper : IHttpClient
    {
        private readonly IHttpClient innerHttpClient;

        public HttpClientWrapper(IHttpClient innerHttpClient)
        {
            this.innerHttpClient = innerHttpClient;
        }

        public string Token { get; set; }

        public void Initialize(IConnection connection)
        {
            innerHttpClient.Initialize(connection);
        }

        public Task<IResponse> Get(string url, Action<IRequest> prepareRequest, bool isLongRunning)
        {
            var urlBuilder = new UrlBuilder(url);
            urlBuilder.QueryString["token"] = Uri.EscapeDataString(Token);
            url = urlBuilder.Build();

            return innerHttpClient.Get(url, prepareRequest, isLongRunning);
        }

        public Task<IResponse> Post(string url, Action<IRequest> prepareRequest, IDictionary<string, string> postData, bool isLongRunning)
        {
            var urlBuilder = new UrlBuilder(url);
            urlBuilder.QueryString["token"] = Uri.EscapeDataString(Token);
            url = urlBuilder.Build();

            return innerHttpClient.Post(url, prepareRequest, postData, isLongRunning);
        }
    }


    internal sealed class UrlBuilder : UriBuilder
    {
        StringDictionary _queryString = null;

        #region Properties

        public StringDictionary QueryString
        {
            get
            {
                if (_queryString == null)
                {
                    _queryString = new StringDictionary();

                    PopulateQueryString();
                }

                return _queryString;
            }
        }

        public string PageName
        {
            get
            {
                string path = base.Path;
                return path.Substring(path.LastIndexOf("/") + 1);
            }
            set
            {
                string path = base.Path;
                path = path.Substring(0, path.LastIndexOf("/"));
                base.Path = string.Concat(path, "/", value);
            }
        }
        #endregion

        #region Constructor overloads

        public UrlBuilder()
            : base()
        {
        }

        public UrlBuilder(string uri)
            : base(uri)
        {
        }

        public UrlBuilder(Uri uri)
            : base(uri)
        {
        }

        public UrlBuilder(string schemeName, string hostName)
            : base(schemeName, hostName)
        {
        }

        public UrlBuilder(string scheme, string host, int portNumber)
            : base(scheme, host, portNumber)
        {
        }

        public UrlBuilder(string scheme, string host, int port, string pathValue)
            : base(scheme, host, port, pathValue)
        {
        }

        public UrlBuilder(string scheme, string host, int port, string path, string extraValue)
            : base(scheme, host, port, path, extraValue)
        {
        }

        #endregion

        #region Public methods

        public string Build()
        {
            GetQueryString();
            return Uri.AbsoluteUri;
        }

        public new string ToString()
        {
            GetQueryString();
            return Uri.AbsoluteUri;
        }

        #endregion

        #region Private methods

        private void PopulateQueryString()
        {
            string query = base.Query;
            if (query == string.Empty || query == null)
            {
                return;
            }
            query = query.Substring(1); //remove the ?

            _queryString.Clear();

            string[] pairs = query.Split(new char[] { '&' });
            foreach (string s in pairs)
            {
                string[] pair = s.Split(new char[] { '=' });
                _queryString[pair[0]] = pair[1];
            }
        }

        private void GetQueryString()
        {
            int count = _queryString.Count;

            string[] keys = new string[count];
            string[] values = new string[count];
            string[] pairs = new string[count];

            _queryString.Keys.CopyTo(keys, 0);
            _queryString.Values.CopyTo(values, 0);

            for (int i = 0; i < count; i++)
            {
                pairs[i] = string.Concat(keys[i], "=", values[i]);
            }

            base.Query = string.Join("&", pairs);
        }
        #endregion
    }
}
