﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NUnit.Framework;

namespace RB.Stats.Data.Receiver.Tests
{
    [TestFixture]
    public class MarketTradesReceiver
    {
        [Test]
        public async Task MarketTrades_Start()
        {
            Common.AuthClient authClient = new Common.AuthClient("http://release-test.rtsboard.ru/", "password", "P5_trader1", "16iaLccU");
            DynamicDataFeed df = new DynamicDataFeed("http://release-test.rtsboard.ru/", "marketTrades", authClient);

            df.Connect();

            await df.StartAsync();
            Assert.AreEqual(df.ReceiverState, Common.ReceiverState.Run);

            df.Stop();
            Assert.AreEqual(df.ReceiverState, Common.ReceiverState.Stoped);
        }

        [Test]
        public async Task MarketTrades_StartAndSubscribe()
        {
            Common.AuthClient authClient = new Common.AuthClient("http://release-test.rtsboard.ru/", "password", "P5_trader1", "16iaLccU");
            DynamicDataFeed df = new DynamicDataFeed("http://release-test.rtsboard.ru/", "marketTrades", authClient);

            df.Connect();
            await df.StartAsync();

            df.Subscribe("testMarketTrades", new Newtonsoft.Json.Linq.JObject());

            System.Threading.Thread.Sleep(3000);

            Assert.Greater(df.GetAddedDataContainer().Count, 0);

            df.Stop();
        }

        [Test]
        public async Task MarketTrades_StartAndSubscribeAndUpdateSubscribe()
        {
            Common.AuthClient authClient = new Common.AuthClient("http://release-test.rtsboard.ru/", "password", "P5_trader1", "16iaLccU");
            DynamicDataFeed df = new DynamicDataFeed("http://release-test.rtsboard.ru/", "marketTrades", authClient);

            df.Connect();
            await df.StartAsync();

            System.Threading.Thread.Sleep(3000);
            df.Subscribe("testMarketTrades", new Newtonsoft.Json.Linq.JObject());

            System.Threading.Thread.Sleep(3000);
            df.UpdateSubscribe(new Newtonsoft.Json.Linq.JObject());

            df.Stop();
        }

    }
}
