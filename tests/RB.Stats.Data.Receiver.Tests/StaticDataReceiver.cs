﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NUnit.Framework;

namespace RB.Stats.Data.Receiver.Tests
{
    [TestFixture]
    public class StaticDataReceiver
    {
        [Test]
        public async Task StaticData_StaticData()
        {
            Common.AuthClient authClient = new Common.AuthClient("http://release-test.rtsboard.ru/", "password", "P5_trader1", "16iaLccU");
            authClient.GetTokens();

            StaticDataFeed staticFeed = new StaticDataFeed(authClient);
            bool result = false;

            result = (bool)(await staticFeed.GetMarketSectorsAsync())["success"];
            Assert.IsTrue(result);

            result = (bool)(await staticFeed.GetInstrumentClasses())["success"];
            Assert.IsTrue(result);

            result = (bool)(await staticFeed.GetInstruments())["success"];
            Assert.IsTrue(result);

            result = (bool)(await staticFeed.GetCurrencyRates("USD", DateTime.Now.AddDays(-3)))["success"];
            Assert.IsTrue(result);
            
        }

        [Test]
        public async Task StaticData_QuoteHistoryData()
        {
            Common.AuthClient authClient = new Common.AuthClient("http://test.rtsboard.ru/", "syscredential", "1", "1");
            authClient.GetTokens();

            StaticDataFeed staticFeed = new StaticDataFeed(authClient);
                        
            bool result = (bool)(await staticFeed.GetQuoteHistory("S9DKA7JG1J-QT00-14E2VSD8O6E4G26TEY3MXWGB69"))["success"];
            Assert.IsTrue(result);

            result = (bool)(await staticFeed.GetQuoteRevision("S9DKA7JG1J-QT00-14E2VSD8O6E4G26TEY3MXWGB69", 7))["success"];
            Assert.IsTrue(result);

        }
    }
}
