﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NUnit.Framework;

namespace RB.Stats.Data.Receiver.Tests
{
    [TestFixture]
    public class AllQuotesReceiver
    {
        [Test]
        public async Task AllQuotes_Start()
        {
            Common.AuthClient authClient = new Common.AuthClient("http://release-test.rtsboard.ru/", "syscredential", "1", "1");
            DynamicDataFeed df = new DynamicDataFeed("http://release-test.rtsboard.ru/", "allQuotes", authClient);

            df.Connect();

            await df.StartAsync();
            Assert.AreEqual(df.ReceiverState, Common.ReceiverState.Run);

            df.Stop();
            Assert.AreEqual(df.ReceiverState, Common.ReceiverState.Stoped);
        }

        [Test]
        public async Task AllQuotes_StartAndSubscripe()
        {
            Common.AuthClient authClient = new Common.AuthClient("http://release-test.rtsboard.ru/", "syscredential", "1", "1");
            DynamicDataFeed df = new DynamicDataFeed("http://release-test.rtsboard.ru/", "allQuotes", authClient);

            df.Connect();
            await df.StartAsync();

            df.Subscribe("testAllQuotes", new Newtonsoft.Json.Linq.JObject());

            System.Threading.Thread.Sleep(3000);

            Assert.Greater(df.GetAddedDataContainer().Count, 0);

            df.Stop();
        }

        [Test]
        public async Task AllQuotes_StartAndSubscripeAndUpdateSubscribe()
        {
            Common.AuthClient authClient = new Common.AuthClient("http://release-test.rtsboard.ru/", "syscredential", "1", "1");
            DynamicDataFeed df = new DynamicDataFeed("http://release-test.rtsboard.ru/", "allQuotes", authClient);

            df.Connect();
            await df.StartAsync();

            System.Threading.Thread.Sleep(100);
            df.Subscribe("testAllQuotes", new Newtonsoft.Json.Linq.JObject());

            System.Threading.Thread.Sleep(100);
            df.UpdateSubscribe(new Newtonsoft.Json.Linq.JObject());

            df.Stop();
        }

    }
}
